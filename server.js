//Install express server
var express = require('express');
var forceSsl = require('force-ssl-heroku');

var path = require('path');

var app = express();
app.use(forceSsl);

app.use(forceSsl);

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/frontend'));

app.get('/*', function(req,res) {
    res.sendFile(path.join(__dirname+'/dist/frontend/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
