import { AppPage } from './app.po';
import { browser, by, element, logging, protractor } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display browser title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Pointbar');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

describe('Actor registration and login tests', () => {

  beforeEach(() => {
    browser.get(browser.baseUrl);
    var EC = browser.ExpectedConditions;
    browser.wait(EC.visibilityOf(element(by.id('nav-item-registration'))),10000);
  });

  it('Bar registration form should be valid', async() => {
    // await browser.get(browser.baseUrl);

    // var EC = browser.ExpectedConditions;
    // await browser.wait(EC.visibilityOf(element(by.id('nav-item-registration'))),10000);

    var nav_item_registration = element(by.id('nav-item-registration'));
    nav_item_registration.click();

    var bar_registration_link = element(by.id('bar-registration-nav-item'));
    bar_registration_link.click();

    let form = element(by.id('bar-registration-form'));

    element(by.name('username')).sendKeys('barEjemploE2E');
    element(by.name('password1')).sendKeys('Ejemplo1');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('barEjemploE2E@pointbar.com');
    element(by.name('name')).sendKeys('Bar Ejemplo e2e');
    element(by.name('phone')).sendKeys('123456789');
    element(by.id('terms_and_conditions')).click();

    expect(form.getAttribute('class')).toContain('ng-valid');

    form.submit();
  });

  it('Bar registration form should be invalid (passwords do not match)', async() => {
    // await browser.get(browser.baseUrl);

    // var EC = browser.ExpectedConditions;
    // await browser.wait(EC.visibilityOf(element(by.id('nav-item-registration'))),10000);

    var nav_item_registration = element(by.id('nav-item-registration'));
    nav_item_registration.click();

    var bar_registration_link = element(by.id('bar-registration-nav-item'));
    bar_registration_link.click();

    let form = element(by.id('bar-registration-form'));

    element(by.name('username')).sendKeys('barEjemploE2ENoVale');
    element(by.name('password1')).sendKeys('Ejemplo132');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('barEjemploE2ENoVale@pointbar.site');
    element(by.name('name')).sendKeys('Bar Ejemplo e2e');
    element(by.name('phone')).sendKeys('123456789');
    element(by.id('terms_and_conditions')).click();

    expect(form.getAttribute('class')).toContain('ng-invalid');

    form.submit();
  });

  it('Bar registration form should be invalid (email is wrong)', async() => {
    // await browser.get(browser.baseUrl);

    // var EC = browser.ExpectedConditions;
    // await browser.wait(EC.visibilityOf(element(by.id('nav-item-registration'))),10000);

    var nav_item_registration = element(by.id('nav-item-registration'));
    nav_item_registration.click();

    var bar_registration_link = element(by.id('bar-registration-nav-item'));
    bar_registration_link.click();

    let form = element(by.id('bar-registration-form'));

    element(by.name('username')).sendKeys('barEjemploE2E');
    element(by.name('password1')).sendKeys('Ejemplo1');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('barEjemploE2E');
    element(by.name('name')).sendKeys('Bar Ejemplo e2e');
    element(by.name('phone')).sendKeys('123456789');
    element(by.id('terms_and_conditions')).click();

    expect(form.getAttribute('class')).toContain('ng-invalid');

    form.submit();
  });

  it('Bar registration form should be invalid (phone field is not a number)', async() => {
    // await browser.get(browser.baseUrl);

    // var EC = browser.ExpectedConditions;
    // await browser.wait(EC.visibilityOf(element(by.id('nav-item-registration'))),10000);

    var nav_item_registration = element(by.id('nav-item-registration'));
    nav_item_registration.click();

    var bar_registration_link = element(by.id('bar-registration-nav-item'));
    bar_registration_link.click();

    let form = element(by.id('bar-registration-form'));

    element(by.name('username')).sendKeys('barEjemploE2ENoVale');
    element(by.name('password1')).sendKeys('Ejemplo1');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('barEjemploE2ENoVale@pointbar.site');
    element(by.name('name')).sendKeys('Bar Ejemplo e2e');
    element(by.name('phone')).sendKeys('123456789asd');
    element(by.id('terms_and_conditions')).click();

    expect(form.getAttribute('class')).toContain('ng-invalid');

    form.submit();
  });

  it('Bar registration form should be invalid (terms and conditions checkbox is not checked)', async() => {
    // await browser.get(browser.baseUrl);

    // var EC = browser.ExpectedConditions;
    // await browser.wait(EC.visibilityOf(element(by.id('nav-item-registration'))),10000);

    var nav_item_registration = element(by.id('nav-item-registration'));
    nav_item_registration.click();

    var bar_registration_link = element(by.id('bar-registration-nav-item'));
    bar_registration_link.click();

    let form = element(by.id('bar-registration-form'));

    element(by.name('username')).sendKeys('barEjemploE2ENoVale');
    element(by.name('password1')).sendKeys('Ejemplo1');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('barEjemploE2ENoVale@pointbar.site');
    element(by.name('name')).sendKeys('Bar Ejemplo e2e');
    element(by.name('phone')).sendKeys('123456789');

    expect(form.getAttribute('class')).toContain('ng-invalid');

    form.submit();
  });

  it('Bar login should be valid', async() => {
    await browser.get(browser.baseUrl);

    var EC = browser.ExpectedConditions;
    await browser.wait(EC.visibilityOf(element(by.id('nav-item-login'))),10000);

    var login_link = element(by.id('nav-item-login'));
    login_link.click();

    let formLogin = element(by.id('login-form'));

    element(by.name('username')).sendKeys('JuanPedro');
    element(by.name('password')).sendKeys('sadasdf3f2');

    let button = element(by.id('login-button'));

    button.click().then(function (){
      browser.waitForAngular();
      expect(browser.driver.get('http://localhost:4200/home'));
      //expect(authenticationError.toBe(true));
    });

    var nav_item_settings = element(by.id('nav-item-settings'))
    nav_item_settings.click();

    element(by.id('logout-nav-item')).click()

  });

  it('Client login should be valid', async() => {
    await browser.get(browser.baseUrl);

    var EC = browser.ExpectedConditions;
    await browser.wait(EC.visibilityOf(element(by.id('nav-item-login'))),10000);

    var login_link = element(by.id('nav-item-login'));
    login_link.click();

    let formLogin = element(by.id('login-form'));

    element(by.name('username')).sendKeys('AntonioManuel');
    element(by.name('password')).sendKeys('sadasdf3f2');

    let button = element(by.id('login-button'));

    button.click().then(function (){
      browser.waitForAngular();
      expect(browser.driver.get('http://localhost:4200/home'));
      //expect(authenticationError.toBe(true));
    });

    var nav_item_settings = element(by.id('nav-item-settings'))
    nav_item_settings.click();

    element(by.id('logout-nav-item')).click()
  });

  it('Bar login and employee registration', async() => {
    var login_link = element(by.id('nav-item-login'));
    login_link.click();

    let formLogin = element(by.id('login-form'));

    element(by.name('username')).sendKeys('PointTapa');
    element(by.name('password')).sendKeys('sadasdf3f2');

    let button = element(by.id('login-button'));

    button.click().then(function (){
      browser.waitForAngular();
    });

    // expect(browser.driver.get('http://localhost:4200/home'));

    element(by.id('nav-item-employee-list')).click();

    var EC = browser.ExpectedConditions;
    await browser.wait(EC.visibilityOf(element(by.id('employee-register-button'))),10000);

    element(by.id('employee-register-button')).click();

    await browser.wait(EC.visibilityOf(element(by.id('employee-registration-form'))));

    let form = element(by.id('employee-registration-form'));

    element(by.name('username')).sendKeys('employeeEjemploE2E');
    element(by.name('password1')).sendKeys('Ejemplo1');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('employeeEjemploE2E@pointbar.com');
    element(by.name('name')).sendKeys('Employee Ejemplo e2e');

    var is_working = element.all(by.tagName('option')).get(0);
    browser.wait(EC.visibilityOf(is_working), 5000);
    is_working.click();

    var control_employee = element.all(by.tagName('option')).get(3);
    browser.wait(EC.visibilityOf(control_employee), 5000);
    control_employee.click();

    var location = element.all(by.tagName('option')).get(4);
    browser.wait(EC.visibilityOf(location), 5000);
    location.click();
    element(by.id('terms_and_conditions')).click();

    expect(form.getAttribute('class')).toContain('ng-valid');

    form.submit();

    var nav_item_settings = element(by.id('nav-item-settings'));
    nav_item_settings.click();

    element(by.id('logout-nav-item')).click();

  });


  it('Bar login and employee registration with point control', async() => {
    var login_link = element(by.id('nav-item-login'));
    login_link.click();

    let formLogin = element(by.id('login-form'));

    element(by.name('username')).sendKeys('PointTapa');
    element(by.name('password')).sendKeys('sadasdf3f2');

    let button = element(by.id('login-button'));

    button.click().then(function (){
      browser.waitForAngular();
    });

    // expect(browser.driver.get('http://localhost:4200/home'));

    element(by.id('nav-item-employee-list')).click();

    var EC = browser.ExpectedConditions;
    await browser.wait(EC.visibilityOf(element(by.id('employee-register-button'))),10000);

    element(by.id('employee-register-button')).click();

    await browser.wait(EC.visibilityOf(element(by.id('employee-registration-form'))));

    let form = element(by.id('employee-registration-form'));

    element(by.name('username')).sendKeys('employeeEjemploE2E2');
    element(by.name('password1')).sendKeys('Ejemplo1');
    element(by.name('password2')).sendKeys('Ejemplo1');
    element(by.name('email')).sendKeys('employeeEjemploE2E2@pointbar.com');
    element(by.name('name')).sendKeys('Employee Ejemplo e2e');

    var is_working = element.all(by.tagName('option')).get(0);
    browser.wait(EC.visibilityOf(is_working), 5000);
    is_working.click();

    var control_employee = element.all(by.tagName('option')).get(2);
    browser.wait(EC.visibilityOf(control_employee), 5000);
    control_employee.click();

    browser.wait(EC.visibilityOf(element(by.name('limit'))), 5000);
    element(by.name('limit')).sendKeys(100)

    var location = element.all(by.tagName('option')).get(4);
    browser.wait(EC.visibilityOf(location), 5000);
    location.click();
    element(by.id('terms_and_conditions')).click();

    expect(form.getAttribute('class')).toContain('ng-valid');

    form.submit();

    var nav_item_settings = element(by.id('nav-item-settings'));
    nav_item_settings.click();

    element(by.id('logout-nav-item')).click();

  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
