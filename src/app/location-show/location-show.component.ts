import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LocationService, Location } from "../services/location.service";
import { UserService } from "../services/user.service";
import { PointService } from "../services/point.service";

@Component({
  selector: "app-location-show",
  templateUrl: "./location-show.component.html",
  styleUrls: ["./location-show.component.css"]
})
export class LocationShowComponent implements OnInit {
  location: Location = {
    id: null,
    name: null,
    description: null,
    email: null,
    website: null,
    logo: null,
    photo: null,
    latitude: null,
    longitude: null,
    street: null,
    city: null,
    state: null,
    postal_code: null,
    country: null,
    is_active: null
  };

  points: number = 0;

  constructor(
    private router: Router,
    private locationService: LocationService,
    private pointService: PointService,
    public userService: UserService
  ) {}

  ngOnInit(): void {
    const locationId = window.localStorage.getItem("locationId");
    // ---------------------------
    if (!locationId) {
      alert("Invalid action.");
      this.router.navigate(["home"]);
      return;
    }

    this.locationService.getLocationById(+locationId).subscribe(
      data => {
        this.location = data;
        const ua = UserService.getuser_account();
        if (ua && ua.authority == "CLIENT")
          this.pointService
            .getPointByLoggedClientAndLocationId(this.location.id)
            .subscribe(data => {
              const points = data.map(p => p.quantity);
              if (points.length > 0)
                this.points = points.reduce((a, b) => a + b);
              else this.points = 0;
            }, err => this.router.navigate(["home"]));
      },
      err => {this.router.navigate(["home"]);}
    );
  }

  listSales(location: Location, Type): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    localStorage.removeItem("location_name");
    localStorage.setItem("location_name", location.name);
    localStorage.setItem("type", Type);
    this.router.navigate(["list-sale"]);
  }

  challengeList(location: Location,TypeC): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    localStorage.removeItem("location_name");
    localStorage.setItem("location_name", location.name);
    localStorage.setItem("typec", TypeC);
    this.router.navigate(["challenge-list"]);
  }
}
