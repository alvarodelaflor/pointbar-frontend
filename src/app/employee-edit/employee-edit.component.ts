import { Component, OnInit } from '@angular/core';
import { EmployeeService, Employee } from "../services/employee.service";
import { Location, LocationService } from "../services/location.service";
import { FormGroup, FormBuilder, Validators, ValidationErrors } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { UserService } from '../services/user.service';
import { ModalExportedUser } from "../modals/modal-exported/modal-exported.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalDelete } from "../modals/modal-delete/modal-delete.component";
import { ModalActorUpdate } from "../modals/modal-actor-update/modal-actor-update.component";
import { ModalDeleteUserComponent } from '../modals/modal-delete-user/modal-delete-user.component';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})

export class EmployeeEditComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private employeeService: EmployeeService,
    private locationService: LocationService,
    private translateService: TranslateService,
    private userService: UserService,
    private modalService: NgbModal,
  ) { }

  locations: Location[] = [];
  editForm: FormGroup;
  isSubmitted: boolean = false;
  showLimit: boolean = false;
  showDesc: boolean = false;
  currentLang = this.translateService.currentLang;
  selected: boolean = true;
  isWorking: boolean;
  isNotWorking: boolean;
  control: boolean;
  notControl: boolean;
  rolemp: boolean;
  rolbar: boolean;
  employeeLocation: Location;
  yesnoworking: string;
  yesnocontrol: string;
  locationName: string;

  ngOnInit(): void {

    const emailRegex = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)';
    const employeeId = window.localStorage.getItem("employeeId");

    var user_account = UserService.getuser_account();

    if (user_account) {
      // Permitir editar el perfil al empleado y a su jefe
      // Cliente es el único rol que no puede entrar actualmente
      if (user_account.authority == "CLIENT") {
        this.router.navigate(["home"]);
      }
    } else {
      this.router.navigate(["home"]);
    }

    if (user_account.authority == "BAR") {

      if (employeeId == null) {
        this.router.navigate(["home"]);
      }

      this.locationService.getLocations().subscribe(
        locations => this.locations = locations,
        error => { this.router.navigate(['home']) })
    }

    this.editForm = this.formBuilder.group({

      user_account: this.formBuilder.group({
        username: ["", [Validators.required, Validators.maxLength(30)]],
        email: ["", [Validators.required, Validators.pattern(emailRegex)]]
      }),

      id: [""],
      granted_points: [""],
      bar_field: [""],
      terms_and_conditions: [true],

      name: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      is_working: ["", Validators.required],
      location: ["", Validators.required],
      control: ["", Validators.required],
      limit_points: ["", [Validators.required, Validators.min(0), Validators.max(10000000)]],
    });

    if (user_account.authority == "EMPLOYEE") {

      this.employeeService.getEmployee().subscribe(
        data => {
          this.editForm.setValue(data[0]),

            this.rolemp = user_account.authority == "EMPLOYEE",
            this.rolbar = user_account.authority == "BAR",

            this.isWorking = this.editForm.value.is_working,
            this.isNotWorking = !this.editForm.value.is_working,

            this.control = this.editForm.value.control,
            this.notControl = !this.editForm.value.control,

            this.showLimit = this.control,

            this.yesnoworking = this.changeBoolToString(this.isWorking),
            this.yesnocontrol = this.changeBoolToString(this.control)

        },

        err => {
          this.router.navigate(["home"])
        }
      );

    } else {

      this.employeeService.getEmployeeById(+employeeId).subscribe(
        data => {
          this.editForm.setValue(data),

            this.rolemp = user_account.authority == "EMPLOYEE",
            this.rolbar = user_account.authority == "BAR",

            this.isWorking = this.editForm.value.is_working,
            this.isNotWorking = !this.editForm.value.is_working,

            this.control = this.editForm.value.control,
            this.notControl = !this.editForm.value.control,

            this.showLimit = this.control,

            this.locationName = localStorage.getItem("nameLoc")
        },

        err => {
          this.router.navigate(["home"]);
        }

      );
    }

  }

  // getLocationId() {

  //   this.employeeService.getEmployee().subscribe(
  //     employee => { localStorage.setItem("idLoc", employee[0].location.toString()) })

  //   this.getLocationName();
  // }

  // getLocationName() {

  //   this.locationService.getLocationById(+localStorage.getItem('idLoc')).subscribe(
  //     location => {
  //       localStorage.setItem("nameLoc", location.name)
  //     })
  // }

  changeBoolToString(bool: boolean) {

    var res = ""
    if (!bool) {

      res = "No";
    } else {

      if (this.currentLang == "es") {

        res = "Sí";
      } else {

        res = "Yes";
      }
    }

    return res;
  }

  onSubmit() {
    this.isSubmitted = true;
    var user_account = UserService.getuser_account();
    var value = this.editForm.value;
    if (!this.showLimit) {
      value.limit_points = 0;
    }
    if (this.hasErrors()) return;
    if (user_account.authority == "EMPLOYEE") {
      var user_account = UserService.getuser_account();
      this.employeeService.updateOwnEmployee(this.editForm.value).subscribe(
        data => {
          this.router.navigate(['home']);
          this.openModal(true, "", 3);
        },
        error => {
          this.openModal(false, error.error, 3);
        }
      );
    } else {
      this.employeeService.updateEmployee(this.editForm.value).subscribe(
        data => { this.router.navigate(["employee-list"]) },
        error => { this.router.navigate(["home"]) }
      );
    }
  }

  hasErrors() {

    let hasErrors: boolean = false;

    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.editForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });

    return hasErrors;
  }

  showPointsLimit(event: { value: any; target: { value: string; }; }) {

    if (event.target.value == "True")
      this.showLimit = true;
    else
      this.showLimit = false;
  }

  get formControls() {
    return this.editForm.controls;
  }

  get user_accountControls() {
    var user_account = <FormGroup>this.editForm.get('user_account');
    return user_account.controls;
  }

  onDelete() {

    if (this.hasErrors()) return;

    var user_account = UserService.getuser_account();

    if (user_account.authority == "EMPLOYEE") {

      this.employeeService.deleteEmployeeAnon(this.editForm.value).subscribe(data => {
      
        this.userService.logout();
        this.router.navigate(["home"]);
    },
      error => {
        this.userService.logout();
        this.router.navigate(["home"]);
        this.openModal(false, error.error, 2);
      });

    } else {

      const employeeId = window.localStorage.getItem("employeeId");
      
      this.employeeService.deleteEmployeeBarAnon(this.editForm.value, +employeeId).subscribe(
        data => {
        this.router.navigate(["employee-list"]);
      }, error => {
        this.userService.logout();
        this.router.navigate(["home"]);
        this.openModal(false, error.error, 2);
      })
    }
    
  }

  onExport(id: number) {
    if (this.hasErrors()) return;
    this.employeeService.export(id).subscribe(data => {
      this.openModal(true, "", 1);
    },
      error => {
        this.openModal(false, error.error, 1);
      });
  }

  openModal(success, error_message, id) {
    if (id == 1) {
      var modalRef = this.modalService.open(ModalExportedUser);
    } else if (id == 2) {
      var modalRef = this.modalService.open(ModalDelete);
    } else {
      var modalRef = this.modalService.open(ModalActorUpdate);
    }
    modalRef.componentInstance.success = success;
    var t = typeof error_message;
    if (['boolean', 'number', 'string', 'symbol', 'function'].indexOf(t) == -1) {
      var err_msg = String(error_message.detail);
    } else {
      var err_msg = String(error_message);
    }
    modalRef.componentInstance.error_message = err_msg;
  }

  deleteModal() {
    const modalRef = this.modalService.open(ModalDeleteUserComponent);
    modalRef.result.then((result) => {
      // Close
    }, (reason) => {
      // Delete
      if (reason == "save")
        this.onDelete();
    });
  }

}
