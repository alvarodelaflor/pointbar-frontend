import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LocationCreateComponent } from "./location-create/location-create.component";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HomeComponent } from "./home/home.component";
import { ClientRegisterComponent } from "./client-register/client-register.component";
import { BarRegisterComponent } from "./bar-register/bar-register.component";
import { EmployeeRegisterComponent } from "./employee-register/employee-register.component";
import { ClientEditComponent } from "./client-edit/client-edit.component";
import { LocationListComponent } from "./location-list/location-list.component";
import { PointListComponent } from "./point-list/point-list.component";
import { LocationEditComponent } from "./location-edit/location-edit.component";
import { LoginComponent } from "./login/login.component";
import { BarEditComponent } from "./bar-edit/bar-edit.component";
import { UserService } from "./services/user.service";
import { ModalExportedUser } from "./modals/modal-exported/modal-exported.component";
import { MapComponent } from "./map/map.component";
import { SubscriptionCreateComponent } from "./subscription-create/subscription-create.component";
import { SubscriptionListComponent } from "./subscription-list/subscription-list.component";
import { SubscriptionShowComponent } from "./subscription-show/subscription-show.component";
import { AgmCoreModule, GoogleMapsAPIWrapper } from "@agm/core";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
import { PayloadCreateComponent } from "./payload-create/payload-create.component";
import { ZXingScannerModule } from "@zxing/ngx-scanner";
import { PayloadShowComponent } from "./payload-show/payload-show.component";
import { PayloadScanComponent } from "./payload-scan/payload-scan.component";
import { ExchangeShowComponent } from "./exchange-show/exchange-show.component";
import { ExchangeScanComponent } from "./exchange-scan/exchange-scan.component";
import { PayloadListComponent } from './payload-list/payload-list.component';
import { ExchangeListComponent } from './exchange-list/exchange-list.component';
import { NgxKjuaModule } from "ngx-kjua";
import { ChallengeCreateComponent } from "./challenge-create/challenge-create.component";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptorService } from "./services/auth-interceptor.service";
import { SaleCreateComponent } from "./sale-create/sale-create.component";
import { ChallengeListComponent } from "./challenge-list/challenge-list.component";
import { SaleEditComponent } from "./sale-edit/sale-edit.component";
import { LocationShowComponent } from "./location-show/location-show.component";
import { SaleListAuthorizedComponent } from "./sale-list-authorized/sale-list-authorized.component";
import { ModalActorConfirmComponent } from "./modals/modal-actor-confirm/modal-actor-confirm.component";
import { ModalUserDeleteData } from "./modals/modal-actor-delete/modal-actor-delete.component";
import { ModalDelete } from "./modals/modal-delete/modal-delete.component";
import { ModalActorUpdate } from "./modals/modal-actor-update/modal-actor-update.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TermsAndConditionsComponent } from "./terms-and-conditions/terms-and-conditions.component";
import { AboutComponent } from "./about/about.component";
import { EmployeeListComponent } from "./employee-list/employee-list.component";
import { CompletedChallengesListComponent } from "./completed-challenges-list/completed-challenges-list.component";
import { PaymentSuccessComponent } from "./payment-success/payment-success.component";
import { LocationsNfoundComponent } from "./locations-nfound/locations-nfound.component";
import { ModalChallengeClaimComponent } from "./modals/modal-challenge-claim/modal-challenge-claim.component";
import { ModalChallengeCreateComponent } from "./modals/modal-challenge-create/modal-challenge-create.component";
import { ModalPayloadRefresh} from "./modals/modal-payload-refresh/modal-payload-refresh.component";
import { ModalExchangeRefresh} from "./modals/modal-exchange-refresh/modal-exchange-refresh.component";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { NgxSpinnerModule } from "ngx-spinner";
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { CompletedChallengeShowComponent } from './completed-challenge-show/completed-challenge-show.component';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalUserRegisterComponent } from "./modals/modal-user-register/modal-user-register.component";
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { ModalLocationCreateComponent } from './modals/modal-location-create/modal-location-create.component';
import { ModalDeleteUserComponent } from './modals/modal-delete-user/modal-delete-user.component';
import { ModalCancelSubscriptionComponent } from './modals/modal-cancel-subscription/modal-cancel-subscription.component';
import { ClientAdvertisementComponent } from './client-advertisement/client-advertisement.component';
import { ActivationComponent } from './activation/activation.component';

@NgModule({
  declarations: [
    AppComponent,
    LocationCreateComponent,
    HomeComponent,
    ClientRegisterComponent,
    BarEditComponent,
    BarRegisterComponent,
    EmployeeRegisterComponent,
    ClientEditComponent,
    LocationListComponent,
    LocationEditComponent,
    LoginComponent,
    MapComponent,
    PayloadCreateComponent,
    PayloadShowComponent,
    PayloadScanComponent,
    PayloadListComponent,
    PointListComponent,
    ExchangeListComponent,
    ExchangeShowComponent,
    ExchangeScanComponent,
    SubscriptionCreateComponent,
    SubscriptionListComponent,
    SubscriptionShowComponent,
    ChallengeCreateComponent,
    SaleCreateComponent,
    SaleListAuthorizedComponent,
    ChallengeListComponent,
    SaleEditComponent,
    LocationShowComponent,
    ModalActorConfirmComponent,
    TermsAndConditionsComponent,
    AboutComponent,
    EmployeeListComponent,
    CompletedChallengesListComponent,
    PaymentSuccessComponent,
    LocationsNfoundComponent,
    ModalChallengeClaimComponent,
    NotificationListComponent,
    CompletedChallengeShowComponent,
    ModalUserRegisterComponent,
    ModalExportedUser,
    ModalUserDeleteData,
    ModalPayloadRefresh,
    ModalExchangeRefresh,
    ModalChallengeCreateComponent,
    EmployeeEditComponent,
    ModalLocationCreateComponent,
    ModalDeleteUserComponent,
    ModalDelete,
    ModalActorUpdate,
    ModalCancelSubscriptionComponent,
    ClientAdvertisementComponent,
    ActivationComponent,
  ],
  entryComponents: [
    ModalActorConfirmComponent,
    TermsAndConditionsComponent,
    ModalUserRegisterComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyA-2LY5Iv_zdu5XhC95CjOAJ-hU3EbAyz0"
    }),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule.forRoot({
      defaultLanguage: "en",
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ZXingScannerModule,
    NgxKjuaModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    SatPopoverModule,
    NgbCarouselModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    GoogleMapsAPIWrapper
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
