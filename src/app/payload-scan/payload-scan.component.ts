import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PayloadService, Payload } from '../services/payload.service';
import { EmployeeService, Employee } from '../services/employee.service';
import { PointService, Point } from '../services/point.service';
import { ClientService, Client } from '../services/client.service';
import { UserService} from '../services/user.service';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-payload-scan',
  templateUrl: './payload-scan.component.html',
  styleUrls: ['./payload-scan.component.css']
})
export class PayloadScanComponent implements OnInit {

  payloads: Payload[] = [];
  payload: Payload;
  enableScan: boolean = true;
  qrCode: number;
  alreadyScanned: boolean = false;
  successfullyScanned: boolean = false;
  codeNotFound: boolean = false;
  existCamera: boolean = true;
  scanDeactivated: boolean = false;
  points: number;


  constructor(private formBuilder: FormBuilder,private router: Router, private payloadService: PayloadService, private pointService: PointService, private userService: UserService, private clientService: ClientService) { }

  ngOnInit(): void {

  }

  scanSuccessHandler(event)
  {
    this.enableScan = false;
    this.qrCode = parseInt(event);
    this.payloadService.getPayloadByCode(this.qrCode).subscribe(
      data =>
      {
        this.payloads = data;
        if(this.payloads.length == 0){
          this.codeNotFound = true;
        }else{
          this.payload = this.payloads[0];
          this.updatePayloadAndPoints();
        }
      },
      error =>
      {
        alert("An error ocurred while trying to do the transaction");
      }
    );
  }

  updatePayloadAndPoints(){
    if (this.payload.is_scanned) {
      this.alreadyScanned = true;
    }else{
      this.payloadService.updatePayloadandPoints(this.qrCode).subscribe
      (data =>
        {
          this.points = this.payload.points;
          this.successfullyScanned = true;
      },
      error =>
      {
        alert("An error ocurred while trying to do the transaction");
      });
    }
  }

  camerasNotFoundHandler($event){
    this.existCamera = false;
  }

  disableScan(){
    this.scanDeactivated = !this.scanDeactivated;
  }

  reloadView(){
    this.payloads = [];
    this.payload = undefined;
    this.enableScan = true;
    this.qrCode = 0;
    this.alreadyScanned = false;
    this.successfullyScanned = false;
    this.codeNotFound= false;
    this.existCamera = true;
    this.points = 0;
  }
}
