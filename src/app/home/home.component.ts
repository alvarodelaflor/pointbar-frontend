import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { UserService } from "../services/user.service";
import { MembershipService, Membership} from '../services/membership.service';
import { TranslateService } from '@ngx-translate/core';



declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  memberships: Membership[] = [];

  constructor(private router: Router ,private membershipService: MembershipService, public userService: UserService, public translate: TranslateService) {
  }

  ngOnInit(): void {
    this.getMemberships();
  }

  public loadScript(url: string) {
    const body = document.body as HTMLDivElement;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  getMemberships(): void {
    this.membershipService.getMemberships().subscribe(
      memberships => {
        this.memberships = memberships;
      },
      err => {
        this.router.navigate(["home"]);
      }
    );
  }

  scanQR(): void {
    this.router.navigate(["scan-exchange"]);
  }

  createPayload(): void {
    this.router.navigate(["create-payload"]);
  }

}
