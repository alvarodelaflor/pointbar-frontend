import { ChangeDetectorRef, Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { ClientService } from '../services/client.service';
import { UserService } from '../services/user.service';
import { ModalUserRegisterComponent } from "../modals/modal-user-register/modal-user-register.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";



@Component({
  selector: 'app-client-register',
  templateUrl: './client-register.component.html',
  styleUrls: ['./client-register.component.css'],
})

export class ClientRegisterComponent implements OnInit {
  @ViewChild("photoFile")
  photoVariable: ElementRef;

  constructor(private formBuilder: FormBuilder, private router: Router, private clientService: ClientService, private cd: ChangeDetectorRef, public us: UserService, private modalService: NgbModal) { }

  createForm: FormGroup;
  isSubmitted: boolean = false;

  ngOnInit(): void {
    const phoneRegex = '^[0-9]{9,9}$|^(\\+)[0-9]{11,12}$|^((\\()[0-9]{3,3}(\\)) [0-9]{3}-[0-9]{4,4})$';
    const birth_dateRegex = '[0-9]{4}-[0-9]{2}-[0-9]{2}';
    const emailRegex = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)';

    this.createForm = this.formBuilder.group({
      user_account: this.formBuilder.group({
        username: ['', [Validators.required, Validators.maxLength(30)]],
        password1: ['', [Validators.required, Validators.maxLength(64)]],
        password2: ['', Validators.required],
        email: ['', [Validators.required, Validators.pattern(emailRegex)]]
      }, { validators: this.confirmPassword }),
      terms_and_conditions: [""],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      surname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      phone: ['', [Validators.required, Validators.pattern(phoneRegex)]],
      photo: ['', ],
      birth_date: ['', [Validators.required, Validators.pattern(birth_dateRegex)]]
    }, {validators : [this.check_terms_and_conditions,this.check_birth_date]});
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    var value = this.createForm.value;
    var check = value.terms_and_conditions;
    if (value.terms_and_conditions) {
      value.terms_and_conditions = "True";
    }
    this.clientService.createClient(value).subscribe(data => {
      this.openModal(true, "");
      this.router.navigate(['login']);
    },
    error => {
      // Back-end errors
      const errors = error.error;
      let e = {};
      for (let key in errors) {
        let value = errors[key];
        if (key == 'user_account'){
          for (let k in value) {
            let v = value[k];
            e[k] = v;
          }
        }else{
          e[key] = value;
        }
      }
      if (e['email']){
        if (e['email'] == 'Enter a valid email address.') {
          this.user_accountControls['email'].setErrors({pattern:true});
        } else {
          this.user_accountControls['email'].setErrors({emailUnique:true});
        }
      }
      if (e['username']){
        this.user_accountControls['username'].setErrors({usernameUnique:true});
      }
      if (e['password1']){
        this.user_accountControls['password1'].setErrors({password1Short:true});
      }
    });
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    Object.keys(this.user_accountControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get('user_account').get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  get formControls() { return this.createForm.controls; }

  get user_accountControls() {
    var user_account = <FormGroup>this.createForm.get('user_account');
    return user_account.controls;
  }

  get photo() {
    return this.createForm.value["photo"];
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      if (!file.type.includes("image")) {
        this.photoVariable.nativeElement.value = "";
        this.createForm.value.photo = null;
        return;
      }
      reader.onload = () => {
        this.createForm.patchValue({
          photo: reader.result
        });

        this.cd.markForCheck();
      };
    }
  }

  confirmPassword(formGruop: FormGroup) {
    let password = formGruop.get('password1').value;
    let password2 = formGruop.get('password2').value;
    if (password != password2 && password.length != 0 && password2.length != 0) {
      formGruop.get('password2').setErrors({
        MatchPassword: true
      })
    } else {
      return null
    }
  }

  check_terms_and_conditions(formGruop: FormGroup) {
    let terms_and_conditions = formGruop.get('terms_and_conditions').value;

    if (!terms_and_conditions) {
        formGruop.get('terms_and_conditions').setErrors({
          termsAndConditionsRequired: true
      })
    } else {
      return null
    }
  }





  check_birth_date(formGruop: FormGroup) {
    let birth_date = formGruop.get('birth_date').value;
    let birth_date_aux = birth_date.split("-", 3);
    let day_birth_date = birth_date_aux[2];
    let month_birth_date = birth_date_aux[1];
    let year_birth_date = birth_date_aux[0];

    let date = new Date().toLocaleDateString();
    let date_aux = date.split("/", 3);
    let day_now = date_aux[0];
    let month_now = date_aux[1];
    let year_now = date_aux[2];

    if (month_now.length == 1) {
        month_now = 0 + month_now;
    }

    if (+year_now - year_birth_date  < 13) {
      formGruop.get('birth_date').setErrors({
        ErrorDay: true
      })
    } else {
        if (month_now < month_birth_date && +year_now - year_birth_date  == 13) {
          formGruop.get('birth_date').setErrors({
            ErrorDay: true
          })
        } else {
          if (day_now < day_birth_date && +year_now - year_birth_date  == 13 && month_now == month_birth_date) {
            formGruop.get('birth_date').setErrors({
              ErrorDay: true
            })
          } else {
            return null
          }
      }
    }
  }
  openModal(success, error_message) {
    const modalRef = this.modalService.open(ModalUserRegisterComponent);
    modalRef.componentInstance.success = success;
    modalRef.componentInstance.error_message = error_message;
  }
}



