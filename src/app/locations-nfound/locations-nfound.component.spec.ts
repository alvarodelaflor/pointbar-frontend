import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationsNfoundComponent } from './locations-nfound.component';
import { AppRoutingModule } from '../app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from '../app.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('LocationsNfoundComponent', () => {
  let component: LocationsNfoundComponent;
  let fixture: ComponentFixture<LocationsNfoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppRoutingModule, HttpClientModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
        }
      })],
      declarations: [ LocationsNfoundComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsNfoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
