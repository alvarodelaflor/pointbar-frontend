import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-locations-nfound',
  templateUrl: './locations-nfound.component.html',
  styleUrls: ['./locations-nfound.component.css']
})
export class LocationsNfoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
