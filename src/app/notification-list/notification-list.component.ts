import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService, Notification} from '../services/notification.service';
import { UserService} from '../services/user.service';
import { environment } from "../../environments/environment";


@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {


  unread_count = 0;

  notificationsUnread: Notification[] = [];
  notificationsRead: Notification[] = []; 
  unreadURL = environment.apiUrl + "/api/unread_list/";
  readURL = environment.apiUrl + "/api/read_list/";
  nextUnread: string;
  nextRead: string;
  isLoading: boolean = false;
  isLoading2: boolean = false;

  constructor(private router: Router, private notificationService: NotificationService, public userService: UserService) { }

  ngOnInit(): void {
    /* this.getNotifications();
    this.getNotificationsRead(); */
    this.getUnreadNotifications(this.unreadURL);
    this.getReadNotifications(this.readURL);
  }

  // getNotifications() {
  //   this.notificationService.getNotifications().subscribe(
  //     object => {
  //       this.unread_count = object['unread_count'];
  //       this.notificationsUnread = object['unread_list'];
  //     },
  //     err => {this.router.navigate(['home']); }
  //   );
  // }

  // getNotificationsRead() {
  //   this.notificationService.getNotificationsRead().subscribe(
  //     object => {
  //       this.notificationsRead = object['all_list'].filter(x => x.unread === false);
  //     },
  //     err => {this.router.navigate(['home']); }
  //   );
  // }

  markAsRead(id: number) {
    this.notificationService.markAsRead(id).subscribe(
      x => {
        this.getUnreadNotifications(this.unreadURL);
        this.getReadNotifications(this.readURL);
      }
    );
  }

  delete(id: number) {
    this.notificationService.delete(id).subscribe(
      x => {
        this.getUnreadNotifications(this.unreadURL);
        this.getReadNotifications(this.readURL);
      }
    );
  }
  getUnreadNotifications(url: string) {
    this.isLoading = true;
    this.notificationService.getPaginatedNotifications(url).subscribe(response => {
      if (response.next != null) {
        this.nextUnread = response.next;
      } else {
        this.nextUnread = "null";
      }
      this.notificationsUnread = response.results;

      this.isLoading = false;
    }, err => {this.router.navigate(["home"]);});
  }
  getReadNotifications(url: string) {
    this.isLoading2 = true;
    this.notificationService.getPaginatedNotifications(url).subscribe(response => {
      if (response.next != null) {
        this.nextRead = response.next;
      } else {
        this.nextRead = "null";
      }
      this.notificationsRead = response.results;

      this.isLoading2 = false;
    }, err => {this.router.navigate(["home"]);});
  }

  fetchNextUnread() {
    
    console.log("unread");
    if (this.nextUnread != "null" && !this.isLoading) {
      this.getNextUnreadPage(this.nextUnread);
    }
  }
  fetchNextRead() {

    if (this.nextRead != "null" && !this.isLoading2) {
      this.getNextReadPage(this.nextRead);
    }
  }

  getNextUnreadPage(url: string) {
    this.isLoading = true;
    this.notificationService.getPaginatedNotifications(url).subscribe(response => {
      if (response.next != null) {
        this.nextUnread = response.next;
      } else {
        this.nextUnread = "null";
      }
      this.notificationsUnread = this.notificationsUnread.concat(response.results);

      this.isLoading = false;
    }, err => {this.router.navigate(["home"]);});
  }
  getNextReadPage(url: string) {
    this.isLoading2 = true;
    this.notificationService.getPaginatedNotifications(url).subscribe(response => {
      if (response.next != null) {
        this.nextRead = response.next;
      } else {
        this.nextRead = "null";
      }
      this.notificationsRead = this.notificationsRead.concat(response.results);

      this.isLoading2 = false;
    }, err => {this.router.navigate(["home"]);});
  }

}
