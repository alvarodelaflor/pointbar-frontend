import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionService, SubscriptionRegistration, Subscription } from '../services/subscription.service';
import { Location, LocationService } from '../services/location.service';
import { Membership, MembershipService } from '../services/membership.service';
import { UserService } from '../services/user.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

declare var paypal;


@Component({
  selector: 'app-subscription-create',
  templateUrl: './subscription-create.component.html',
  styleUrls: ['./subscription-create.component.css']
})
export class SubscriptionCreateComponent implements OnInit {

  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;

  locations: Location[] = [];
  memberships: Membership[] = [];
  membershipObtain: Membership;
  show:boolean;
  paypal_subscription_id:string;

  constructor(private translate:TranslateService,private formBuilder: FormBuilder, private router: Router, private locationService: LocationService, private subscriptionService: SubscriptionService, private membershipService: MembershipService, private userService: UserService) { }

  model: SubscriptionRegistration = {
    renovate: true,
    location: '',
    membership: ''
  }

  createForm: FormGroup;
  isSubmitted: boolean = false;
  renovate: boolean = false;
  planId: string;
  membershipToForm: Membership;
  currentLang:string = this.translate.currentLang

  ngOnInit(): void {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = this.translate.currentLang;
    }, err => this.router.navigate(["home"]));

    this.locationService.getLocationsWithoutSubscriptions().subscribe(
      locations => {
        this.locations = locations;
        paypal
          .Buttons({
            createSubscription: (data, actions) => {
              return actions.subscription.create({
                plan_id: this.membershipToForm.subscriptionId
              });
            },
            onApprove: async (data, actions) => {
              this.paypal_subscription_id = data.subscriptionID;
              this.onSubmit();
            },

            onError: err => {
              console.log(err);
            }
          })
          .render(this.paypalElement.nativeElement);

        if (!this.userService.hasAuthority("BAR")) {
          this.router.navigate(["home"]);
        }

        this.membershipService.getMemberships().subscribe(
          memberships => (this.memberships = memberships),
          error => {
            alert("Could not load membership");
            this.router.navigate(["home"]);
          }
        );
      },
      error => {
        alert("Could not load location");
        this.router.navigate(["home"]);
      }
    );
  }

  onSubmit() {


    const subscription = {
      renovate: this.model.renovate, location: { id: this.model.location }, membership: { id: this.membershipToForm.id }, paypal_subscription_id: this.paypal_subscription_id
    }
    this.subscriptionService.createSubscription(subscription).subscribe(data => {
      this.router.navigate(['payment-success']);
    },
      error => {
        // Back-end errors
        const errors = error.error;
        let msg = 'Back-end errors';
        for (let key in errors) {
          let value = errors[key];
          msg += '\n' + key + ' -> ' + value;
        }
        alert(msg);
      });

  }

  get formControls() {
    return this.createForm.controls;
  }

  get formControls_membership() {
    var membership_control = <FormGroup>this.createForm.get('membership');
    return membership_control.controls;
  }

  get formControls_location() {
    var location_control = <FormGroup>this.createForm.get('location');
    return location_control.controls;
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    Object.keys(this.formControls_location).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get('location').get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    Object.keys(this.formControls_membership).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get('membership').get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }
}
