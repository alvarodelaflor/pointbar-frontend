import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionService, Subscription } from '../services/subscription.service';
import { LocationService, Location } from '../services/location.service';
import { UserService } from '../services/user.service';
import { environment } from '../../environments/environment';
import { NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-subscription-list',
  templateUrl: './subscription-list.component.html',
  styleUrls: ['./subscription-list.component.css']
})
export class SubscriptionListComponent implements OnInit {

  subscriptions: Subscription[] = [];
  locationsNoSubscription: Location[] = [];
  itemsURL= environment.apiUrl + "/api/subscription/pagination";
  next: string;
  previous: string;
  isLoading: boolean = false;
  empty: boolean = false;

  constructor(private userService: UserService, private router: Router, private subscriptionService: SubscriptionService, private spinner: NgxSpinnerService, private locationService: LocationService) { }

  ngOnInit(): void {
    if (!this.userService.hasAuthority('BAR')) {
      this.router.navigate(['home'])
    }
    this.setItems(this.itemsURL);
  }

  showSubscription(subscription: Subscription){
    window.localStorage.removeItem('showSubscriptionId');
    window.localStorage.setItem('showSubscriptionId', subscription.id.toString());

    this.router.navigate(['show-subscription']);
  }

  changeStatus(subscription: Subscription): void {
    this.subscriptionService.deleteSubscription(subscription.id).subscribe();
  }

  setItems(url: string) {
    this.isLoading = true;
    this.empty = false;
    this.subscriptionService.getPaginatedSubscriptions(url).subscribe(response => {
      if (response.next !=null) {
        this.next = response.next;
        this.spinner.hide();
      }else{
        this.next = "null";
        this.spinner.hide();
      }
      this.subscriptions =this.subscriptions.concat(response.results);

      if (this.subscriptions.length <= 0) {
        this.empty = true;
      }

      if (response.previous) {
        // set the components previous property here from the response
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, err => this.router.navigate(["home"]));
  }

  fetchNext() {
    if(this.next != "null" && !this.isLoading){
      this.spinner.show();
      this.setItems(this.next);
    }
  }

  checkRedirect():void{

    this.locationService.getLocationsWithoutSubscriptions().subscribe(
      locations => {if (locations.length==0) this.router.navigate(["locations-nfound"]); else this.router.navigate(["create-subscription"])}
    );
  }

}

