import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ExchangeService, Exchange } from "../services/exchange.service";
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalExchangeRefresh} from "../modals/modal-exchange-refresh/modal-exchange-refresh.component";

@Component({
  selector: 'app-exchange-show',
  templateUrl: './exchange-show.component.html',
  styleUrls: ['./exchange-show.component.css']
})
export class ExchangeShowComponent implements OnInit {
  qrCode: string;
  points: string;
  scanned: boolean = false;

  constructor(private router: Router,  private exchangeService: ExchangeService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.qrCode = window.localStorage.getItem('createdExchangeHash');
    this.points = window.localStorage.getItem('createdExchangePoints');
    window.localStorage.removeItem('createdExchangeHash');
    window.localStorage.removeItem('createdExchangePoints');
    this.checkIfScanned();
  }

  private async checkIfScanned(){
    var i = 0;
    while(i < 500){
      await this.delay(3000);
      this.exchangeService.getExchangeByCode(parseInt(this.qrCode)).subscribe(data => {
        if(data[0].is_scanned){
          this.scanned = true;
        }
      })
      if(this.scanned){
        break;
      }
      i++;
    }
    this.router.navigate(['list-sale']);
    this.openModal();
  }

  openModal() {
    var modalRef = this.modalService.open(ModalExchangeRefresh);
    modalRef.componentInstance.success = true;
  }

  private delay(ms: number){
  return new Promise(resolve => setTimeout(resolve, ms));
  }

  return(){
    this.router.navigate(['list-sale']);
  }
}
