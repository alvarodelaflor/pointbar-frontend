import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, SubscriptionService } from '../services/subscription.service';
import { UserService } from '../services/user.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalDeleteUserComponent } from '../modals/modal-delete-user/modal-delete-user.component';
import { ModalDelete } from '../modals/modal-delete/modal-delete.component';
import { ModalCancelSubscriptionComponent } from '../modals/modal-cancel-subscription/modal-cancel-subscription.component';


@Component({
  selector: 'app-subscription-show',
  templateUrl: './subscription-show.component.html',
  styleUrls: ['./subscription-show.component.css']
})
export class SubscriptionShowComponent implements OnInit {

  subscription: Subscription={
    id:null,
    expiration_date:null,
    renovate:false,
    is_active:false,
    membership:{id:null,
      titleS: null,
      descriptionS: null,
      titleE: null,
      descriptionE: null,
      price: null,
      membership_type: null,
      subscriptionId:null},
    location:{id:null,
      name:null,
      description: null,
      email: null,
      website: null,
      logo: null,
      photo: null,
      latitude: null,
      longitude: null,
      street: null,
      city: null,
      state: null,
      postal_code: null,
      country: null,
      is_active: null}
  };

  constructor(private modalService: NgbModal,private translate:TranslateService,private userService: UserService, private router: Router, private subscriptionService: SubscriptionService) { }

  currentLang:string = this.translate.currentLang

  ngOnInit(): void {

    var id = window.localStorage.getItem("showSubscriptionId");
    if (!id) this.router.navigate(["home"]);
    this.translate.onLangChange
      .subscribe((event: LangChangeEvent) => {
        this.currentLang=this.translate.currentLang;
    }, err => this.router.navigate(["home"]));

    if (!this.userService.hasAuthority('BAR')) {
      this.router.navigate(['home'])
    }

    this.subscriptionService.getSubscriptionById(id).subscribe(subscription => this.subscription = subscription, error => { alert("Could not load subscription"); this.router.navigate(["home"]);});

//    if (UserService.getuser_account().user_id != this.subscription.getUser().getId()) {
//      this.router.navigate(['home'])
//    }
  }

  deleteSubscription(subscription: Subscription){
    // We have to cache the id to communicate with edit component
    this.subscriptionService.deleteSubscription(subscription.id).subscribe(data => {
      alert('Autorenovate canceled!');
      this.subscription.renovate = false;
      this.router.navigate(['show-subscription']);
      },
      error => {
      // Back-end errors
      const errors = error.error;
      let msg = 'Back-end errors';
      for (let key in errors) {
        let value = errors[key];
        msg += '\n' + key + ' -> ' + value;
      }
      alert(msg);
    });
  }

  activeSubscription(subscription: Subscription){
    // We have to cache the id to communicate with edit component
    this.subscriptionService.activateSubscription(subscription).subscribe(data => {
      alert('Autorenovate activate!');
      this.router.navigate(['show-subscription']);
      },
      error => {
      // Back-end errors
      const errors = error.error;
      let msg = 'Back-end errors';
      for (let key in errors) {
        let value = errors[key];
        msg += '\n' + key + ' -> ' + value;
      }
      alert(msg);
    });
  }

  deleteModal(subscription:Subscription) {
    const modalRef = this.modalService.open(ModalCancelSubscriptionComponent);
    modalRef.result.then((result) => {
      // Close
    }, (reason) => {
      // Delete
      if (reason == "save")
        this.onDelete(subscription);
    });
  }

  onDelete(subscription) {

    this.subscriptionService.deleteSubscription(subscription.id).subscribe(data => {
      this.router.navigate(["list-subscriptions"]);
    },
      error => {
        this.router.navigate(["home"]);
        this.openModal(false, error.error);
      });
  }

  openModal(success, error_message) {

    var modalRef = this.modalService.open(ModalDelete);

    modalRef.componentInstance.success = success;
    var t = typeof error_message;
    if (['boolean', 'number', 'string', 'symbol', 'function'].indexOf(t) == -1){
      var err_msg = String(error_message.detail);
    } else {
      var err_msg = String(error_message);
    }
    modalRef.componentInstance.error_message = err_msg;
  }
}
