import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ActivationService } from '../services/activation.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private activationService: ActivationService,
    private router: Router
    ) { }

  token:String = ""; 
  uid:String = "";
  fail:boolean = true;


  ngOnInit(): void {
    this.token = this.route.snapshot.paramMap.get("token");
    this.uid = this.route.snapshot.paramMap.get("uid");
    console.log(this.token);
    console.log(this.uid);
    this.activationService.activateAccount(this.uid, this.token).subscribe(
      data => {
        console.log(data);
        console.log(data['success']);
        this.fail = !data['success'];
      },
      err => {this.router.navigate(["home"]);}
    );
    
  }

}
