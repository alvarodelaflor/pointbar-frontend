import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExchangeService, Exchange } from '../services/exchange.service';
import { EmployeeService, Employee } from '../services/employee.service';
import { PointService, Point } from '../services/point.service';
import { ClientService, Client } from '../services/client.service';
import { Location } from '../services/location.service';
import { UserService } from '../services/user.service';
import { SubscriptionService } from '../services/subscription.service';
import { environment } from "../../environments/environment";
import { timer } from 'rxjs';
import * as CanvasJS from '../../assets/canvasjs.min.js';
import { TranslateService } from '@ngx-translate/core';
import { Sale } from '../services/sale.service';
import { variable } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-exchange-list',
  templateUrl: './exchange-list.component.html',
  styleUrls: ['./exchange-list.component.css']
})

export class ExchangeListComponent implements OnInit {

  exchanges: Exchange[] = [];
  createForm: FormGroup;
  hasVipSubscription: boolean = true;
  exchangesURL = environment.apiUrl + "/api/exchange/bar";
  next: string;
  previous: string;
  isLoading: boolean = false;
  currentLang: string; ç
  chartOneColumns;
  chartTwoColumns;
  chartThreePie;
  titleFirstChart: string;
  titleSecondChart: string;
  titleThirdChart: string;


  constructor(private translate: TranslateService, private formBuilder: FormBuilder, private router: Router, private exchangeService: ExchangeService, private subscriptionService: SubscriptionService, public userService: UserService, private clientService: ClientService) { this.translate.onLangChange.subscribe(event => this.updateTitles()); }

  ngOnInit(): void {

    if (this.userService.hasAuthority('BAR')) {
      this.updateTitlesOnInit();
    }

    this.createForm = this.formBuilder.group({
      location: [""],
      employee: [""],
      client: [""],
      sale: [""],
      create_moment_min: [""],
      create_moment_max: [""],
      create_moment_exact: [""],
      points_min: [""],
      points_max: [""],
      points_exact: [""],
    });

    this.getExchanges(this.exchangesURL);

    if (this.userService.hasAuthority('BAR')) {
      this.subscriptionService.getVipSubscriptionForLogggedBar().subscribe(
        data => {
          if (!data || data.length == 0) {
            this.hasVipSubscription = false;
          }
        },
        err => { this.router.navigate(["home"]); }
      );
    }
  }
  getExchanges(url: string) {
    var location = this.createForm.get("location").value;
    if (location == "") {
      location = "none";
    }

    var employee = this.createForm.get("employee").value;
    if (employee == "") {
      employee = "none";
    }

    var client = this.createForm.get("client").value;
    if (client == "") {
      client = "none";
    }

    var sale = this.createForm.get("sale").value;
    if (sale == "") {
      sale = "none";
    }

    var create_moment_min = this.createForm.get("create_moment_min").value;
    if (create_moment_min == "") {
      create_moment_min = "none";
    }

    var create_moment_max = this.createForm.get("create_moment_max").value;
    if (create_moment_max == "") {
      create_moment_max = "none";
    }

    var create_moment_exact = this.createForm.get("create_moment_exact").value;
    if (create_moment_exact == "") {
      create_moment_exact = "none";
    }

    var points_min = this.createForm.get("points_min").value;
    if (points_min == "") {
      points_min = "none";
    }

    var points_max = this.createForm.get("points_max").value;
    if (points_max == "") {
      points_max = "none";
    }

    var points_exact = this.createForm.get("points_exact").value;
    if (points_exact == "") {
      points_exact = "none";
    }

    var x = new Date();
    var currentTimeZoneOffsetInHours = x.getTimezoneOffset() / 60;

    var query = "?location=" + location + "&employee=" + employee + "&client=" + client + "&sale=" + sale + "&create_moment_min=" + create_moment_min + "&create_moment_max=" + create_moment_max + "&create_moment_exact=" + create_moment_exact + "&points_min=" + points_min + "&points_max=" + points_max + "&points_exact=" + points_exact + "&gap=" + currentTimeZoneOffsetInHours;
    this.isLoading = true;
    this.exchangeService.getPaginatedExchangesFilterBar(url, query).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
      } else {
        this.next = "null";
      }
      this.exchanges = response.results;

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;

      this.compileCharts(this.exchanges);
    }, error => { this.router.navigate(["home"]) });
  }
  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.getNextPage(this.next);
    }
  }
  getNextPage(url: string) {
    this.isLoading = true;
    this.exchangeService.getPaginatedExchangesFilterBar(url, "").subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
      } else {
        this.next = "null";
      }
      this.exchanges = this.exchanges.concat(response.results);

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;

      this.compileCharts(this.exchanges);
    }, error => { this.router.navigate(["home"]) });
  }


  compileCharts(exchanges): void {

    //Location por número de sales

    var chartOne = [];

    exchanges.reduce(function (res, value) {
      if (!res[value.sale.location.name]) {
        res[value.sale.location.name] = { name: value.sale.location.name, count: 0 };
        chartOne.push(res[value.sale.location.name])
      }
      res[value.sale.location.name].count += 1;
      return res;
    }, {});

    //Employee por número de sales

    var chartTwo = [];

    exchanges.reduce(function (res, value) {
      if (!res[value.employee.name]) {
        res[value.employee.name] = { name: value.employee.name, count: 0 };
        chartTwo.push(res[value.employee.name])
      }
      res[value.employee.name].count += 1;
      return res;
    }, {});

    //Sales más canjeadas

    var chartThree = [];

    exchanges.reduce(function (res, value) {
      var key = value.sale.title + '-' + value.is_scanned;
      if (!res[key]) {
        res[key] = { name: value.sale.title, scanned: value.is_scanned, count: 0 };
        chartThree.push(res[key])
      }
      res[key].count += 1;
      return res;
    }, {});

    var chartThreeSplitted = chartThree.filter(x => x.scanned == true);





    chartOne.sort((a, b) => (b.count > a.count) ? 1 : ((a.count > b.count) ? -1 : 0));
    console.log('Componentes del primer chart:', chartOne);



    chartTwo.sort((a, b) => (b.count > a.count) ? 1 : ((a.count > b.count) ? -1 : 0));
    console.log('Componentes del segundo chart:', chartTwo);

    chartThree.sort((a, b) => (b.count > a.count) ? 1 : ((a.count > b.count) ? -1 : 0));
    console.log('Componentes del tercer chart:', chartThreeSplitted);


    var chartLocationSalesCanjeadas = [];
    var chartEmployeeSalesCanjeadas = [];
    var chartSalesMasCanjeadas = [];

    for (let i = 0; i < chartOne.length; i++) {


      chartLocationSalesCanjeadas[i] = {
        y: chartOne[i].count,
        label: chartOne[i].name

      };

    }

    for (let j = 0; j < chartTwo.length; j++) {


      chartEmployeeSalesCanjeadas[j] = {
        y: chartTwo[j].count,
        label: chartTwo[j].name

      };

    }

    for (let x = 0; x < chartThreeSplitted.length; x++) {

      chartSalesMasCanjeadas[x] = {
        y: chartThreeSplitted[x].count,
        label: chartThreeSplitted[x].name

      };
    }

    this.chartOneColumns = new CanvasJS.Chart("chartColumnsContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.titleFirstChart,
        fontFamily: "arial",
        fontWeight: "bold"
      },
      data: [{
        type: "column",
        dataPoints: chartLocationSalesCanjeadas
      }]
    });


    this.chartTwoColumns = new CanvasJS.Chart("chartTwoColumnsContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.titleSecondChart,
        fontFamily: "arial",
        fontWeight: "bold"
      },
      data: [{
        type: "column",
        dataPoints: chartEmployeeSalesCanjeadas
      }]
    });

    this.chartThreePie = new CanvasJS.Chart("chartThreePieContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.titleThirdChart,
        fontFamily: "arial",
        fontWeight: "bold"
      },
      data: [{
        type: "pie",
        dataPoints: chartSalesMasCanjeadas
      }]
    });


    if (exchanges.length != 0) {

      this.chartOneColumns.render();

      this.chartTwoColumns.render();

      this.chartThreePie.render();
    }


  }

  updateTitlesOnInit(): void {

    if (this.translate.currentLang == "en") {
      this.titleFirstChart = "Sales per location";
      this.titleSecondChart = "Sales per employee";
      this.titleThirdChart = "Challenging sales";

    } else {
      this.titleFirstChart = "Ofertas por cada establecimiento";
      this.titleSecondChart = "Ofertas por cada trabajador";
      this.titleThirdChart = "Ofertas más canjeadas";

    }
  }


  updateTitles(): void {

    this.updateTitlesOnInit();



    if (this.chartOneColumns != null) {
      this.chartOneColumns.title.set("text", this.titleFirstChart);
      this.chartTwoColumns.title.set("text", this.titleSecondChart);
      this.chartThreePie.title.set("text", this.titleThirdChart);
    }

  }

  showLocation(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["location-show"]);
  }

}


