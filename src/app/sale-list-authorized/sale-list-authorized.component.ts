import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SaleService, Sale } from "../services/sale.service";
import { ExchangeService, Exchange } from "../services/exchange.service";
import { PointService, Point } from "../services/point.service";
import { UserService } from "../services/user.service";
import { environment } from "../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";
import { SubscriptionService } from "../services/subscription.service";
import { LocationService } from '../services/location.service';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: "app-sale-list-authorized",
  templateUrl: "./sale-list-authorized.component.html",
  styleUrls: ["./sale-list-authorized.component.css"]
})
export class SaleListAuthorizedComponent implements OnInit {
  sales: Sale[] = [];
  empty = false;
  idLocation: any = window.localStorage.getItem("locationId");
  points: Point[] = [];
  pointQuantity: number;
  exchanges: Exchange[] = [];
  type = localStorage.getItem("type");
  itemsURL =
    environment.apiUrl + "/api/" + this.type + "/" + this.idLocation + "/";
  next: string;
  previous: string;
  isLoading: boolean = false;
  sale_button_disabled: boolean = true;
  suscription_msg: string = "checking_for_subscription";
  location_name: any;
  is_owner: boolean = false;
  constructor(
    private router: Router,
    private saleService: SaleService,
    private exchangeService: ExchangeService,
    public userService: UserService,
    public translateService: TranslateService,
    private pointService: PointService,
    private spinner: NgxSpinnerService,
    private subscriptionService: SubscriptionService,
    private locationService: LocationService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.empty = false;
    var type = localStorage.getItem("type");
    this.location_name = window.localStorage.getItem("location_name");
    if (!this.location_name) this.router.navigate(["home"]);
    this.idLocation = window.localStorage.getItem("locationId");
    if (this.userService.hasAuthority("CLIENT")) {
      this.getClientPointsInLocation();
    }
    const ua = UserService.getuser_account();
    if (ua && ua.authority == "BAR") {
      this.locationService.getLocations().subscribe(locations => {
        let loc = locations.filter(l => l.id == this.idLocation);
        this.is_owner = loc.length > 0;
        if (this.is_owner){
          this.subscriptionService.getSubscriptions().subscribe(
            data => {
              this.sale_button_disabled =
                data.filter(e => e.location.id == this.idLocation).length == 0;
              if (this.sale_button_disabled == false) {
                this.suscription_msg = "active_subscription_found";
                this.setItems(this.itemsURL);
              } else {
                this.suscription_msg = "active_subscription_not_found";
              }
            },
            err => this.router.navigate(["home"])
          );
        } else{
          this.setItems(this.itemsURL);
        }
      }, err => this.router.navigate(["home"]));
  } else {
      this.setItems(this.itemsURL);
    }
  }

  getClientPointsInLocation() {
    this.pointService
      .getPointByLoggedClientAndLocationId(this.idLocation)
      .subscribe(data => {
        this.points = data;
        if (this.points.length == 0) {
          this.pointQuantity = 0;
        } else {
          this.pointQuantity = this.points[0].quantity;
        }
      }, err => this.router.navigate(["home"]));
  }

  showSale(sale: Sale) {
    // We have to cache the id to communicate with edit component
    window.localStorage.removeItem("showSaleId");
    window.localStorage.setItem("showSaleId", sale.id.toString());
    // ----------------------------------------------------------
    this.router.navigate(["show-sale"]);
  }

  exchange(id) {
    this.exchangeService.createExchangeBySaleId(id).subscribe(data => {
      this.exchanges = data;
      window.localStorage.setItem(
        "createdExchangeHash",
        this.exchanges[0].qr_code
      );
      window.localStorage.setItem(
        "createdExchangePoints",
        this.exchanges[0].points + ""
      );
      this.router.navigate(["show-exchange"]);
    });
  }

  createSale(): void {
    this.router.navigate(["create-sale"]);
  }

  editSale(sale: Sale) {
    // We have to cache the id to communicate with edit component
    window.localStorage.removeItem("editSaleId");
    window.localStorage.setItem("editSaleId", sale.id.toString());
    // ----------------------------------------------------------
    this.router.navigate(["edit-sale"]);
  }

  deleteSale(sale: Sale) {
    // We have to cache the id to communicate with edit component
    this.saleService.deleteSale(sale).subscribe(data => {
      this.sales = this.sales.filter(b => b.id != sale.id);
      alert('Delete!');
      },
      error => {
        // Back-end errors
        const errors = error.error;
        let msg = "Back-end errors";
        for (let key in errors) {
          let value = errors[key];
          msg += "\n" + key + " -> " + value;
        }
        alert(msg);
      }
    );
  }

  public notEnoughPoints(salePoints) {
    if (salePoints > this.pointQuantity) {
      return true;
    } else {
      return false;
    }
  }

  setItems(url: string) {
    this.empty = false;
    this.isLoading = true;
    this.saleService.getPaginatedSales(url).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
        this.spinner.hide();
      } else {
        this.next = "null";
        this.spinner.hide();
      }
      this.sales = this.sales.concat(response.results);

      if (response.previous) {
        // set the components previous property here from the response
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, err => {
        {}
      }).add(() => {
      if (this.sales.length < 1) {
        this.empty = true;
      } else {
        this.empty = false;
      }
    });;
  }

  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.spinner.show();
      this.setItems(this.next);
    }
  }


  listSales(Type): void {
    this.empty = false;
    localStorage.setItem("type", Type);
    this.sales = [];
    this.type = Type;
    const url = environment.apiUrl + '/api/' + Type + '/' + this.idLocation + '/';
    this.itemsURL = url;
    const ua = UserService.getuser_account();
    if (ua && ua.authority == "BAR") {
      this.locationService.getLocations().subscribe(locations => {
        let loc = locations.filter(l => l.id == this.idLocation);
        this.is_owner = loc.length > 0;
        if (this.is_owner){
          this.subscriptionService.getSubscriptions().subscribe(
            data => {
              this.sale_button_disabled =
                data.filter(e => e.location.id == this.idLocation).length == 0;
              if (this.sale_button_disabled == false) {
                this.suscription_msg = "active_subscription_found";
                this.setItems(url);
              } else {
                this.suscription_msg = "active_subscription_not_found";
              }
            },
            err => this.router.navigate(["home"])
          );
        }else{
          this.setItems(url);
        }
      }, err => this.router.navigate(["home"]));

    } else {
      this.setItems(url);
    }
  }
}
