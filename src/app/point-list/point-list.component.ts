import { Component, OnInit } from "@angular/core";
import { PointService, Point } from "../services/point.service";
import { Location } from "../services/location.service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-point-list",
  templateUrl: "./point-list.component.html",
  styleUrls: ["./point-list.component.css"]
})
export class PointListComponent implements OnInit {
  points: Point[] = [];
  pointsURL = environment.apiUrl + "/api/point/pagination";
  next: string;
  previous: string;
  isLoading: boolean = false;

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private pointService: PointService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.setPoints(this.pointsURL);
  }



  showLocation(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["location-show"]);
  }

  listSales(location: Location, Type): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    localStorage.removeItem("location_name");
    localStorage.setItem("location_name", location.name);
    localStorage.setItem("type", Type);
    this.router.navigate(["list-sale"]);
  }

  challengeList(location: Location,TypeC): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    localStorage.removeItem("location_name");
    localStorage.setItem("location_name", location.name);
    localStorage.setItem("typec", TypeC);
    this.router.navigate(["challenge-list"]);
  }


  setPoints(url: string) {
    this.isLoading = true;
    this.pointService.getPaginatedPoints(url).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
        this.spinner.hide();
      } else {
        this.next = "null";
        this.spinner.hide();
      }
      this.points = this.points.concat(response.results);

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, err => {this.router.navigate(["home"]);});
  }

  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.spinner.show();
      this.setPoints(this.next);
    }
  }
}
