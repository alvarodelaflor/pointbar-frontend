import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidationErrors,
  FormControl
} from "@angular/forms";
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from "@angular/core";
import { Router } from "@angular/router";
import { SaleService, SaleRegistration } from "../services/sale.service";
import { UploadService } from '../services/upload.service';

@Component({
  selector: "app-sale-create",
  templateUrl: "./sale-create.component.html",
  styleUrls: ["./sale-create.component.css"]
})
export class SaleCreateComponent implements OnInit {
  @ViewChild("myFile")
  myFileVariable: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private saleService: SaleService,
    private uploadService: UploadService
  ) {}

  createForm: FormGroup;
  isSubmitted: boolean = false;
  private cd: ChangeDetectorRef;
  renovate: boolean = false;

  ngOnInit(): void {
    const pointsRegex = '^[0-9]{1,6}';
    var locationId= +localStorage.getItem('locationId')
    const urlRegex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.createForm = this.formBuilder.group({
      title: ['', [Validators.required ,Validators.maxLength(60)]],
      description: ['', [Validators.required ,Validators.maxLength(250)]],
      number_points: ['', [Validators.required, Validators.pattern(pointsRegex)]],
      photo:[""],
      start_date: ['', [Validators.required,  this.dateAfter]],
      expiration_date: ['', [Validators.required,  this.dateAfter]],
      location: this.formBuilder.group ({
        id: locationId,
      }),
    },{ validators: this.dateRange });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) {
      return;
    } else {
      this.saleService.createSale(this.createForm.value).subscribe(
        data => {
          this.router.navigate(["list-sale"]);
        },
        error => {
          // Back-end errors
          const errors = error.error;
          let msg = "Back-end errors";
          for (let key in errors) {
            let value = errors[key];
            msg += "\n" + key + " -> " + value;
          }
          alert(msg);
        }
      );
    }
  }

  get photo() {
    return this.createForm.value.photo;
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let valid = true;
      if (file.size > 2000000){ //2MB
        valid = false;
        let control = this.createForm.get("photo");
        control.setErrors({ size: true });
      }else{
        this.formControls["photo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.myFileVariable.nativeElement.value = "";
        this.createForm.value.photo = null;
        return;
      }
      this.createForm.patchValue({
        photo: "../../assets/img/loading.gif",
      });
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        this.createForm.patchValue({
          photo: data.Location,
        });
      });
    }
  }


  get formControls() {
    return this.createForm.controls;
  }

  get formControls_location() {
    var location_control = <FormGroup>this.createForm.get("location");
    return location_control.controls;
  }

  hasErrors() {
    let hasErrors: boolean = false;
    if (this.createForm.errors != null) {
      hasErrors = true;
    }
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    Object.keys(this.formControls_location).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm
        .get("location")
        .get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  dateAfter(formControl: FormControl) {
    const value = formControl.value;
    const date = new Date(value);
    const curr_date = new Date();
    curr_date.setHours(0, 0, 0, 0);
    if (curr_date > date) {
      return {
        after: true
      };
    } else {
      return null;
    }
  }

  dateRange(formGroup: FormGroup) {
    const start_date_input = formGroup.get("start_date").value;
    const expiration_date_input = formGroup.get("expiration_date").value;
    const start_date = new Date(start_date_input);
    const expiration_date = new Date(expiration_date_input);
    if (start_date >= expiration_date) {
      return {
        date_range: true
      };
    } else {
      return null;
    }
  }
}
