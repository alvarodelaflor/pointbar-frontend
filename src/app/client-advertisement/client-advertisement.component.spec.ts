import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAdvertisementComponent } from './client-advertisement.component';

describe('ClientAdvertisementComponent', () => {
  let component: ClientAdvertisementComponent;
  let fixture: ComponentFixture<ClientAdvertisementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAdvertisementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
