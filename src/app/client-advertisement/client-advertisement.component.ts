import { Component, OnInit } from '@angular/core';
import { Membership, MembershipService } from '../services/membership.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-client-advertisement',
  templateUrl: './client-advertisement.component.html',
  styleUrls: ['./client-advertisement.component.css']
})
export class ClientAdvertisementComponent implements OnInit {

  memberships: Membership[] = [];

  constructor(private router: Router,public translate: TranslateService,public userService: UserService,private membershipService: MembershipService) {}




  ngOnInit(): void {
    this.getMemberships();
  }

  getMemberships(): void {
    this.membershipService.getMemberships().subscribe(
      memberships => {
        this.memberships = memberships;
      },
      err => {
        this.router.navigate(["home"]);
      }
    );
  }

}
