import { Component, OnInit, ViewChild } from "@angular/core";
import { LocationService, Location } from "../services/location.service";
import { AgmCoreModule, Marker } from "@agm/core";
import { Router } from '@angular/router';
import { SubscriptionService } from '../services/subscription.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Url } from 'url';
declare var google: any;

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"]
})
export class MapComponent implements OnInit {
  constructor(private subscriptionService: SubscriptionService, private locationService: LocationService, private router: Router) { }

  lat: number = 37.389437;
  lng: number = -5.984443;
  map: any;
  previous;

  locations: any = [];
  iconBaseMain = [];
  iconAuxiliar: number;
  locationsVIP:any;
  markers = [];

  noPoi = [
    {
      featureType: "poi",
      stylers: [{ visibility: "off" }]
    }
  ];

  ngOnInit(): void {
    this.subscriptionService.getAllSubscriptionsNormal().subscribe(locations => {
      this.locations = locations.map(y => y.location)},err => {this.router.navigate(["home"]);});
  }

  mapReady(event: any) {
    this.map = event;
    this.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(
      document.getElementById("Location")
    );
    this.map.setOptions({ styles: this.noPoi });

    this.subscriptionService.getAllSubscriptionsVIP().subscribe(locations => {
      this.locationsVIP = locations.map(y => y.location);

      for (let i = 0; i < this.locationsVIP.length; i++) {

        this.iconBaseMain[i] = "../../assets/img/principal.png";

      }

    }, err => { });
    this.findMe();
  }

  findMe(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        this.map.setCenter(pos);

        for (let i = 0; i < this.markers.length; i++) {

          this.markers[i].setMap(null);
        }

        var marker = new google.maps.Marker({
          position: pos,
          map: this.map,
          title: "Tu ubicación",
          icon: "../../assets/img/marker.png"
        });

        this.markers.push(marker);
      });
    }
  }

  clickedMarker(infowindow) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

  showLocation(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["location-show"]);
  }

  changeIconOver(i: number) {

    if (this.iconAuxiliar != null) {
      this.iconBaseMain[this.iconAuxiliar] = "../../assets/img/principal.png";
    }
    this.iconBaseMain[i] = "../../assets/img/animate1.gif";
  }



  changeIconOut(i: number) {
    this.iconAuxiliar = i;
    this.iconBaseMain[i] = "../../assets/img/animate2.gif";
  }

  listSales(location: Location, Type): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    localStorage.removeItem("location_name");
    localStorage.setItem("location_name", location.name);
    localStorage.setItem("type", Type);
    this.router.navigate(["list-sale"]);
  }

  challengeList(location: Location,TypeC): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    localStorage.removeItem("location_name");
    localStorage.setItem("location_name", location.name);
    localStorage.setItem("typec", TypeC);
    this.router.navigate(["challenge-list"]);
  }






}
