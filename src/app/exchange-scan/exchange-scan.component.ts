import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ExchangeService, Exchange } from "../services/exchange.service";
import { EmployeeService, Employee } from "../services/employee.service";
import { PointService, Point } from "../services/point.service";
import { ClientService, Client } from "../services/client.service";
import { SaleService, Sale, SaleForScanned } from "../services/sale.service";
import { UserService } from "../services/user.service";
import { environment } from "../../environments/environment";

@Component({
  selector: "app-exchange-scan",
  templateUrl: "./exchange-scan.component.html",
  styleUrls: ["./exchange-scan.component.css"]
})
export class ExchangeScanComponent implements OnInit {
  exchanges: Exchange[] = [];
  exchange: Exchange;
  employees: Employee[] = [];
  employee: Employee;
  enableScan: boolean = false;
  qrCode: number;
  alreadyScanned: boolean = false;
  successfullyScanned: boolean = false;
  existCamera: boolean = true;
  codeNotFound: boolean = false;
  notEnoughPoints: boolean = false;
  isAbleToScan: boolean = true;
  saleIncorrect: boolean = false;
  hasLocation: boolean = true;
  noSuscriptionInLocation: boolean = false;
  sales: Sale[] = [];
  sale: Sale;
  title: string;
  description;
  scanDeactivated: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private exchangeService: ExchangeService,
    private pointService: PointService,
    private clientService: ClientService,
    private saleService: SaleService,
    public userService: UserService,
    private employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    if (!this.userService.hasAuthority("EMPLOYEE")) {
      this.router.navigate(["home"]);
    }
    this.employeeService.getEmployeeLogged().subscribe(
      data => {
        this.employees = data;
        this.isAbleToScan = this.employees[0].is_working;
        this.hasLocation = (this.employees[0].location != undefined);
        this.enableScan = this.isAbleToScan && this.hasLocation;
      },
      err => this.router.navigate(["home"])
    );
  }

  scanSuccessHandler(event) {
    this.enableScan = false;
    this.qrCode = parseInt(event);
    this.exchangeService.getExchangeByCode(this.qrCode).subscribe(
      data => {
        this.exchanges = data;
        if (this.exchanges.length == 0) {
          this.codeNotFound = true;
        } else {
          this.exchange = this.exchanges[0];
          var locationIdByEmployee = this.employees[0].location + "";
          var locationIdBySale = this.exchange.sale.location.id + "";
          if (locationIdByEmployee == locationIdBySale) {
            this.updateExchangeAndPoints();
          } else {
            this.saleIncorrect = true;
          }
        }
      },
      error => {
        alert("Error");
        this.router.navigate(["home"]);
      }
    );
  }

  updateExchangeAndPoints() {
    if (this.exchange.is_scanned) {
      this.alreadyScanned = true;
    } else {
      this.exchangeService.updateExchangeandPoints(this.qrCode).subscribe(
        data => {
          this.sales = data;
          if (this.sales.length == 0) {
            this.notEnoughPoints = true;
          } else if (this.sales.length == 2) {
            this.noSuscriptionInLocation = true;
          } else {
            this.title = this.sales[0].title;

            this.description = this.sales[0].description;
            this.successfullyScanned = true;
          }
        },
        error => {
          alert("Error");
          this.router.navigate(["home"]);
        }
      );
    }
  }

  camerasNotFoundHandler($event) {
    this.existCamera = false;
  }

  disableScan(){
    this.scanDeactivated = !this.scanDeactivated;
  }

  reloadView() {
    if (!this.userService.hasAuthority("EMPLOYEE")) {
      this.router.navigate(["home"]);
    }
    this.employeeService.getEmployeeLogged().subscribe(data => {
      this.employees = data;
      this.enableScan =
        this.employees[0].is_working && this.employees[0].location != undefined;
      this.isAbleToScan = this.enableScan;
      this.exchanges = [];
      this.exchange = undefined;
      this.qrCode = 0;
      this.alreadyScanned = false;
      this.successfullyScanned = false;
      this.existCamera = true;
      this.codeNotFound = false;
      this.notEnoughPoints = false;
      this.sales = [];
      this.sale = undefined;
      this.saleIncorrect = false;
      this.noSuscriptionInLocation = false;
      this.title = "";
      this.description = "";
    });
  }
}
