import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PayloadService, Payload } from "../services/payload.service";
import { UserService } from "../services/user.service";
import { EmployeeService, Employee } from "../services/employee.service";
import { ClientService, ClientPayload } from "../services/client.service";
import { environment } from "../../environments/environment";
import {TranslateService} from '@ngx-translate/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalSendPoints } from "../modals/modal-send-points/modal-send-points.component";

@Component({
  selector: "app-payload-create",
  templateUrl: "./payload-create.component.html",
  styleUrls: ["./payload-create.component.css"]
})
export class PayloadCreateComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private clientService: ClientService, private router: Router, private payloadService: PayloadService, private employeeService: EmployeeService, private userService: UserService, private translateService: TranslateService, private modalService: NgbModal) { }

  createForm: FormGroup;
  employees: Employee[] = [];
  employee: Employee;
  points: number;
  radomNumber: string;
  createMoment: Date;
  qr_code: string;
  money: number;
  is_scanned: boolean = false;
  isAbleToPayload: boolean = true;
  hasLocation: boolean = true;
  noSuscriptionInLocation: boolean = false;
  hasErrors: boolean = true;
  maxPointsReached: boolean = false;
  clients: ClientPayload[] = [];
  clientUrl = environment.apiUrl + "/api/client/all/";
  clientUsername: string;
  clientName: string;
  clientSurname: string;
  sentPoints: boolean = false;
  next: string;
  previous: string;
  isLoading: boolean = false;

  ngOnInit(): void {
    if (!this.userService.hasAuthority("EMPLOYEE")) {
      this.router.navigate(["home"]);
    }
    this.employeeService.getEmployeeLogged().subscribe(
      data => {
        this.employees = data;
        this.isAbleToPayload = this.employees[0].is_working;
        this.hasLocation = (this.employees[0].location != undefined);
      },
      err => this.router.navigate(["home"])
    );

    const numberRegex = "^[0-9]+(.[0-9]{1,2})?$";
    this.createForm = this.formBuilder.group({
      amount: new FormControl("0.00", [
        Validators.required,
        Validators.min(0.01),
        Validators.max(999999.99),
        Validators.pattern(numberRegex)
      ]),
      clientSearcher: [""]
    });
    this.getAllClients();
  }

  onSubmit() {
    this.hasErrors = false;
    this.money = this.createForm.value.amount;
    this.points = Math.round(this.money * 100);
    this.radomNumber = Math.round(Math.random() * 1000000000000000).toString();
    this.qr_code = this.radomNumber;
    this.getLoggedEmployee();
  }

  getLoggedEmployee() {
    this.employeeService.getEmployeeLogged().subscribe(data => {
      this.employees = data;
      this.employee = this.employees[0];
      this.createPayload();
    });
  }

  createPayload() {
    this.createForm = this.formBuilder.group({
      points: this.points,
      qr_code: this.qr_code,
      money: this.money,
      create_moment: new Date(),
      scanned_moment: null,
      employee: this.formBuilder.group({
        id: this.employee.id
      }),
      client: this.formBuilder.group({}),
      is_scanned: this.is_scanned
    });
    this.payloadService.createPayload(this.createForm.value).subscribe(data => {
        if(this.employee.control && ((this.employee.granted_points + this.points) > this.employee.limit_points)){
          this.maxPointsReached = true;
        }else{
          window.localStorage.setItem('createdPayloadHash', this.radomNumber);
          window.localStorage.setItem('createdPayloadPoints', this.points + '');
          this.router.navigate(['show-payload']);
        }

      }, error => {
        this.noSuscriptionInLocation = true;
      }
    );
  }

  get formControls() {
    return this.createForm.controls;
  }

  return(){

    this.is_scanned = false;
    this.isAbleToPayload = true;
    this.hasLocation = true;
    this.noSuscriptionInLocation = false;
    this.hasErrors = true;
    this.maxPointsReached = false;
    this.sentPoints = false;
    this.ngOnInit();
    
  }

  addMoney(amount) {
    if (this.createForm.value.amount != null) {
      var valueToSet =
        parseFloat(this.createForm.value.amount) + parseFloat(amount);
    } else {
      var valueToSet = parseFloat(amount);
    }
    this.createForm.get("amount").setValue(valueToSet);
  }

  resetMoney(amount) {
    var valueToSet = parseFloat(amount);
    this.createForm.get("amount").setValue(valueToSet);
  }

  formHasErrors(){
    this.hasErrors = this.createForm.errors.length > 0;
  }

  getAllClients(){

    var client = this.createForm.get("clientSearcher").value;
    if (client == "") {
      client = "none";
    }

    var query = "?username=" + client;
    this.clientService.getAllClients(this.clientUrl, query).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
      } else {
        this.next = "null";
      }
      this.clients = response.results;

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, err => {
      // this.router.navigate(["home"]);
    });
  }

  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.getNextPage(this.next);
    }
  }
  getNextPage(url: string) {
    this.isLoading = true;
    this.clientService.getAllClients(url, "").subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
      } else {
        this.next = "null";
      }
      this.clients = this.clients.concat(response.results);

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, error => { this.router.navigate(["home"]) });
  }

  sendPoints($username, $name, $surname){
    this.money = this.createForm.value.amount;
    this.points = this.money * 100;

    this.clientUsername = $username;
    this.clientName = $name;
    this.clientSurname = $surname;

    this.employeeService.getEmployeeLogged().subscribe(data => {
      this.employees = data;
      this.employee = this.employees[0];
    });

    this.payloadService.sendPoints($username, this.money).subscribe(data =>{
      if(this.employee.control && ((this.employee.granted_points + this.points) > this.employee.limit_points)){
        this.maxPointsReached = true;
      }else{
        this.sentPoints = true;
      }

    }, error => {
      this.noSuscriptionInLocation = true;
    }
    );
  }

}
