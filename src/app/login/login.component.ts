import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidationErrors
} from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  createForm: FormGroup;
  isSubmitted: boolean = false;

  ngOnInit(): void {
    this.createForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    var value = this.createForm.value;
    this.userService.login(value).subscribe(
      data => {
        this.userService.updateData(data);
        if (UserService.getuser_account().authority == "CLIENT")
          this.router.navigate(["map"])
        else this.router.navigate(["home"]);
      },
      error => {
        const errors = error.error;
        this.formControls["password"].setErrors({ loginError: true });
      }
    );
  }

  get formControls() {
    return this.createForm.controls;
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null ) {
        if (!controlErrors.loginError)
          hasErrors = true;
      }
    });
    return hasErrors;
  }

  onKey(event) {
    this.formControls["password"].setErrors(null);
  }
}
