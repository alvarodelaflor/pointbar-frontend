import { Component, OnInit, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { UserService } from '../services/user.service';
import { Location, LocationService } from '../services/location.service';
import { TranslateService } from "@ngx-translate/core";
import { ModalUserRegisterComponent } from "../modals/modal-user-register/modal-user-register.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-employee-register',
  templateUrl: './employee-register.component.html',
  styleUrls: ['./employee-register.component.css']
})
export class EmployeeRegisterComponent implements OnInit {

  locations: Location[] = [];

  constructor(private formBuilder: FormBuilder,
    private router: Router, private employeeService: EmployeeService,
    private locationService: LocationService, private translateService: TranslateService, private modalService: NgbModal) { }

  createForm: FormGroup;
  isSubmitted: boolean = false;
  currentLang = this.translateService.currentLang;
  show_desc: boolean = false;

  ngOnInit(): void {
    const emailRegex = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)';

    this.locationService.getLocations().subscribe(locations => this.locations = locations, error => { this.router.navigate(['home']) })

    this.createForm = this.formBuilder.group({
      user_account: this.formBuilder.group({
        username: ['', [Validators.required, Validators.maxLength(30)]],
        password1: ['', [Validators.required, Validators.maxLength(64)]],
        password2: ['', Validators.required],
        email: ['', [Validators.required, Validators.pattern(emailRegex)]]
      }, { validators: this.confirmPassword }),
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      is_working: ['', Validators.required],
      location: ['', Validators.required],
      terms_and_conditions: [''],
      control: ['', Validators.required],
      limit_points: [0, [Validators.required, Validators.min(0), Validators.max(10000000)]],
    }, { validators: this.check_terms_and_conditions });

    var user_account = UserService.getuser_account();
    if (user_account) {
      if (user_account.authority == "EMPLOYEE" || user_account.authority == "CLIENT") {
        this.router.navigate(['home']);
      }
    } else {
      this.router.navigate(['home']);
    }

  }

  show: boolean = false;
  showPointsLimit(event: { value: any; target: { value: string; }; }) {

    if (event.target.value == "True")
      this.show = true;
    else
      this.show = false;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    var value = this.createForm.value;
    value.location = String(value.location);
    if (value.terms_and_conditions) {
      value.terms_and_conditions = "True";
    } else {
      this.formControls['terms_and_conditions'].setErrors({ termsAndConditions: true });
    }
    if (!this.show) {
      value.limit_points = 0;
    }
    this.employeeService.createEmployee(value).subscribe(data => {
      this.openModal(true, "");
      this.router.navigate(["employee-list"]);
    },
      error => {
        // Back-end errors
        const errors = error.error;
        let e = {};
        for (let key in errors) {
          let value = errors[key];
          if (key == 'user_account') {
            for (let k in value) {
              let v = value[k];
              e[k] = v;
            }
          } else {
            e[key] = value;
          }
        }
        if (e['email']) {
          if (e['email'] == 'Enter a valid email address.') {
            this.user_accountControls['email'].setErrors({ pattern: true });
          } else {
            this.user_accountControls['email'].setErrors({ emailUnique: true });
          }
        }
        if (e['username']) {
          this.user_accountControls['username'].setErrors({ usernameUnique: true });
        }
        if (e['password1']) {
          this.user_accountControls['password1'].setErrors({ password1Short: true });
        }
      });
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    Object.keys(this.user_accountControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get('user_account').get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  get formControls_location() {
    var location_control = <FormGroup>this.createForm.get('location');
    return location_control.controls;
  }

  get formControls() { return this.createForm.controls; }

  get user_accountControls() {
    var user_account = <FormGroup>this.createForm.get('user_account');
    return user_account.controls;
  }

  confirmPassword(formGruop: FormGroup) {
    let password = formGruop.get('password1').value;
    let password2 = formGruop.get('password2').value;

    if (password != password2 && password.length != 0 && password2.length != 0) {
      formGruop.get('password2').setErrors({
        MatchPassword: true
      })
    } else {
      return null
    }
  }

  check_terms_and_conditions(formGruop: FormGroup) {
    let terms_and_conditions = formGruop.get('terms_and_conditions').value;

    if (!terms_and_conditions) {
      formGruop.get('terms_and_conditions').setErrors({
        termsAndConditionsRequired: true
      })
    } else {
      return null
    }
  }

  openModal(success, error_message) {
    const modalRef = this.modalService.open(ModalUserRegisterComponent);
    modalRef.componentInstance.success = success;
    modalRef.componentInstance.error_message = error_message;
  }
}
