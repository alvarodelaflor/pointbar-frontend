import { Component, OnInit } from '@angular/core';
import { Bar, BarService } from '../services/bar.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalActorUpdate } from "../modals/modal-actor-update/modal-actor-update.component";
import { ModalDelete } from "../modals/modal-delete/modal-delete.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalExportedUser } from "../modals/modal-exported/modal-exported.component";
import { ModalDeleteUserComponent } from '../modals/modal-delete-user/modal-delete-user.component';

@Component({
  selector: 'app-bar-edit',
  templateUrl: './bar-edit.component.html',
  styleUrls: ['./bar-edit.component.css']
})
export class BarEditComponent implements OnInit {

  bar: Bar = {
    name: "",
    phone: "",
    terms_and_conditions: "",
    user_account: {
      username: "",
      password1: "",
      password2: "",
      email: "",
      user_id: ""
    },
  };
  editForm: FormGroup;
  isSubmitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private barService: BarService,
    public translate: TranslateService,
    private userService: UserService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    const phoneRegex = '^[0-9]{9,9}$|^(\\+)[0-9]{11,12}$|^((\\()[0-9]{3,3}(\\)) [0-9]{3}-[0-9]{4,4})$';

    this.editForm = this.formBuilder.group({
      user_account: this.formBuilder.group({
        username: ['', [Validators.required, Validators.maxLength(30)]],
        email: ['', [Validators.required]]
      }),
      id: [''],
      terms_and_conditions: true,
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      phone: ['', [Validators.required, Validators.pattern(phoneRegex)]]
    });

    var user_account = UserService.getuser_account();
    if (user_account) {
      if (user_account.authority == "EMPLOYEE" || user_account.authority == "CLIENT") {
        this.router.navigate(['home']);
      }
    } else {
      this.router.navigate(['home']);
    }

    this.barService.getBar().subscribe(
      data => this.editForm.setValue(data[0]),
      err => {this.router.navigate(["home"])}
    );

    this.barService.getBar().subscribe(
      data => this.bar = data[0],
      err => {this.router.navigate(["home"])}
    );

  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    this.barService.updateBar(this.editForm.value).subscribe(data => {
      this.router.navigate(['home']);
      this.openModal(true,"", 3);
    },
      error => {
        this.openModal(false, error.error, 3);
      });
  }

  onDelete() {
    if (this.hasErrors()) return;

    this.barService.deleteBarAnon(this.editForm.value).subscribe(data => {
      this.userService.logout();
      this.router.navigate(["home"]);
    },
      error => {
        this.userService.logout();
        this.router.navigate(["home"]);
        this.openModal(false, error.error, 2);
      });
  }

  onExport(id: number) {
    if (this.hasErrors()) return;
    this.barService.export(id).subscribe(data => {
      this.openModal(true, "", 1);
    },
      error => {
        this.openModal(false, error.error, 1);
      });
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.editForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  get formControls() { return this.editForm.controls; }

  openModal(success, error_message, id) {
    if(id == 1){
       var modalRef = this.modalService.open(ModalExportedUser);
    } else if (id == 2){
       var modalRef = this.modalService.open(ModalDelete);
    } else {
       var modalRef = this.modalService.open(ModalActorUpdate);
    }
    modalRef.componentInstance.success = success;
    var t = typeof error_message;
    if (['boolean', 'number', 'string', 'symbol', 'function'].indexOf(t) == -1){
      var err_msg = String(error_message.detail);
    } else {
      var err_msg = String(error_message);
    }
    modalRef.componentInstance.error_message = err_msg;
  }

  deleteModal() {
    const modalRef = this.modalService.open(ModalDeleteUserComponent);
    modalRef.result.then((result) => {
      // Close
    }, (reason) => {
      // Delete
      if (reason == "save")
        this.onDelete();
    });
  }

}
