import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPayloadRefresh } from './modal-payload-refresh.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/app.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ModalPayloadRefresh', () => {
  let component: ModalPayloadRefresh;
  let fixture: ComponentFixture<ModalPayloadRefresh>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, AppRoutingModule, HttpClientModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
        }
      })],
      declarations: [ ModalPayloadRefresh ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPayloadRefresh);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
