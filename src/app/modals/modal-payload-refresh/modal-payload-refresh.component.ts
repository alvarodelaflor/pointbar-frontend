import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-payload-refresh",
  templateUrl: "./modal-payload-refresh.component.html",
  styleUrls: ["./modal-payload-refresh.component.css"]
})
export class ModalPayloadRefresh implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
