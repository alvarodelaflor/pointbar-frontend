import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-user-register",
  templateUrl: "./modal-user-register.component.html",
  styleUrls: ["./modal-user-register.component.css"]
})
export class ModalUserRegisterComponent implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
