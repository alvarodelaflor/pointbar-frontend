import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-location-create',
  templateUrl: './modal-location-create.component.html',
  styleUrls: ['./modal-location-create.component.css']
})
export class ModalLocationCreateComponent implements OnInit {

  location: any;

  constructor(public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

}
