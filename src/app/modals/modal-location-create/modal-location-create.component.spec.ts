import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalLocationCreateComponent } from './modal-location-create.component';

describe('ModalLocationCreateComponent', () => {
  let component: ModalLocationCreateComponent;
  let fixture: ComponentFixture<ModalLocationCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalLocationCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLocationCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
