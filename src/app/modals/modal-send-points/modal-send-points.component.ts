import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-send-points",
  templateUrl: "./modal-send-points.component.html",
  styleUrls: ["./modal-send-points.component.css"]
})
export class ModalSendPoints implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
