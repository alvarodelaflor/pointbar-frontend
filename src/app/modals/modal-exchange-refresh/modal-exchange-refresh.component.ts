import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-exchange-refresh",
  templateUrl: "./modal-exchange-refresh.component.html",
  styleUrls: ["./modal-exchange-refresh.component.css"]
})
export class ModalExchangeRefresh implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
