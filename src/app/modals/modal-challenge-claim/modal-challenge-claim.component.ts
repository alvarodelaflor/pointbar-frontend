import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-challenge-claim",
  templateUrl: "./modal-challenge-claim.component.html",
  styleUrls: ["./modal-challenge-claim.component.css"]
})
export class ModalChallengeClaimComponent implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
