import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-actor-confirm',
  templateUrl: './modal-actor-confirm.component.html',
  styleUrls: ['./modal-actor-confirm.component.css']
})
export class ModalActorConfirmComponent implements OnInit {

  constructor(public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

}
