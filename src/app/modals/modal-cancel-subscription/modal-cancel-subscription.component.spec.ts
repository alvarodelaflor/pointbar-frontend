import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCancelSubscriptionComponent } from './modal-cancel-subscription.component';

describe('ModalCancelSubscriptionComponent', () => {
  let component: ModalCancelSubscriptionComponent;
  let fixture: ComponentFixture<ModalCancelSubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCancelSubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCancelSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
