import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-cancel-subscription',
  templateUrl: './modal-cancel-subscription.component.html',
  styleUrls: ['./modal-cancel-subscription.component.css']
})
export class ModalCancelSubscriptionComponent implements OnInit {

  constructor(public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

}
