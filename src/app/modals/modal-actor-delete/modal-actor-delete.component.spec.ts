import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUserDeleteData } from './modal-actor-delete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from '../../app.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('UserExportData', () => {
  let component: ModalUserDeleteData;
  let fixture: ComponentFixture<ModalUserDeleteData>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, AppRoutingModule, HttpClientModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
        }
      })],
      declarations: [ ModalUserDeleteData ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUserDeleteData);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
