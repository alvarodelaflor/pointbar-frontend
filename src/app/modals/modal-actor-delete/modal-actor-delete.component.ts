import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-actor-delete',
  templateUrl: './modal-actor-delete.component.html',
  styleUrls: ['./modal-actor-delete.component.css']
})
export class ModalUserDeleteData implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
