import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-exported",
  templateUrl: "./modal-exported.component.html",
  styleUrls: ["./modal-exported.component.css"]
})
export class ModalExportedUser implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
