import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-actor-update",
  templateUrl: "./modal-actor-update.component.html",
  styleUrls: ["./modal-actor-update.component.css"]
})
export class ModalActorUpdate implements OnInit {
  success: boolean = false;
  error_message: string = "";

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}
}
