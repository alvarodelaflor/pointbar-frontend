import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDelete } from './modal-delete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/app.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ModalDelete', () => {
  let component: ModalDelete;
  let fixture: ComponentFixture<ModalDelete>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, AppRoutingModule, HttpClientModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
        }
      })],
      declarations: [ ModalDelete ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDelete);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
