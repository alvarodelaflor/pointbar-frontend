import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-challenge-create',
  templateUrl: './modal-challenge-create.component.html',
  styleUrls: ['./modal-challenge-create.component.css']
})
export class ModalChallengeCreateComponent implements OnInit {

  challenge: any;

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {
  }

}
