import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalChallengeCreateComponent } from './modal-challenge-create.component';

describe('ModalChallengeCreateComponent', () => {
  let component: ModalChallengeCreateComponent;
  let fixture: ComponentFixture<ModalChallengeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalChallengeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalChallengeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
