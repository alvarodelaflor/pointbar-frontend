import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PayloadService, Payload } from '../services/payload.service';
import { EmployeeService, Employee } from '../services/employee.service';
import { PointService, Point } from '../services/point.service';
import { Location } from '../services/location.service';
import { ClientService, Client } from '../services/client.service';
import { UserService } from '../services/user.service';
import { SubscriptionService } from '../services/subscription.service';
import { environment } from "../../environments/environment";
import { timer } from 'rxjs';
import * as CanvasJS from '../../assets/canvasjs.min.js';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-payload-list',
  templateUrl: './payload-list.component.html',
  styleUrls: ['./payload-list.component.css']
})

export class PayloadListComponent implements OnInit {

  payloads: Payload[] = [];
  createForm: FormGroup;
  hasVipSubscription: boolean = true;
  payloadsURL = environment.apiUrl + "/api/payload/bar";
  next: string;
  previous: string;
  isLoading: boolean = false;
  currentLang: string;
  titleFirstChart: string;
  titleSecondChart: string;
  titleThirdChart: string;
  chartOneColumns;
  chartTwoColumns;
  chartThreePyramid;
  compile1: boolean = false;
  compile3: boolean = false;


  constructor(private translate: TranslateService, private formBuilder: FormBuilder, private router: Router, private payloadService: PayloadService, private subscriptionService: SubscriptionService, public userService: UserService, private clientService: ClientService) { this.translate.onLangChange.subscribe(event => this.updateTitles()); }



  ngOnInit(): void {

    if (this.userService.hasAuthority('BAR')) {
      this.updateTitlesOnInit();
    }

    this.createForm = this.formBuilder.group({
      location: [""],
      employee: [""],
      client: [""],
      create_moment_min: [""],
      create_moment_max: [""],
      create_moment_exact: [""],
      points_min: [""],
      points_max: [""],
      points_exact: [""],
    });

    this.getPayloads(this.payloadsURL);

    if (this.userService.hasAuthority('BAR')) {
      this.subscriptionService.getVipSubscriptionForLogggedBar().subscribe(
        data => {
          if (!data || data.length == 0) {
            this.hasVipSubscription = false;
          }
        }, err => this.router.navigate(["home"])
      );
    }
  }
  getPayloads(url: string) {
    var location = this.createForm.get("location").value;
    if (location == "") {
      location = "none";
    }

    var employee = this.createForm.get("employee").value;
    if (employee == "") {
      employee = "none";
    }

    var client = this.createForm.get("client").value;
    if (client == "") {
      client = "none";
    }

    var client = this.createForm.get("client").value;
    if (client == "") {
      client = "none";
    }

    var create_moment_min = this.createForm.get("create_moment_min").value;
    if (create_moment_min == "") {
      create_moment_min = "none";
    }

    var create_moment_max = this.createForm.get("create_moment_max").value;
    if (create_moment_max == "") {
      create_moment_max = "none";
    }

    var create_moment_exact = this.createForm.get("create_moment_exact").value;
    if (create_moment_exact == "") {
      create_moment_exact = "none";
    }

    var points_min = this.createForm.get("points_min").value;
    if (points_min == "") {
      points_min = "none";
    }

    var points_max = this.createForm.get("points_max").value;
    if (points_max == "") {
      points_max = "none";
    }

    var points_exact = this.createForm.get("points_exact").value;
    if (points_exact == "") {
      points_exact = "none";
    }

    var x = new Date();
    var currentTimeZoneOffsetInHours = x.getTimezoneOffset() / 60;

    var query = "?location=" + location + "&employee=" + employee + "&client=" + client + "&create_moment_min=" + create_moment_min + "&create_moment_max=" + create_moment_max + "&create_moment_exact=" + create_moment_exact + "&points_min=" + points_min + "&points_max=" + points_max + "&points_exact=" + points_exact + "&gap=" + currentTimeZoneOffsetInHours;
    this.payloadService.getPaginatedPayloadsFilterBar(url, query).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
      } else {
        this.next = "null";
      }
      this.payloads = response.results;


      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;


      this.compileCharts(this.payloads);



      }, error => { this.router.navigate(["home"]); });
    
  }

  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.getNextPage(this.next);
    }
  }
  getNextPage(url: string) {
    this.isLoading = true;
    this.payloadService.getPaginatedPayloadsFilterBar(url, "").subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
      } else {
        this.next = "null";
      }
      this.payloads = this.payloads.concat(response.results);

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;

      this.compileCharts(this.payloads);

    }, error => { this.router.navigate(["home"]) });
  }



  compileCharts(payloads): void {

    //Location con dinero acumulado

    var chartOne = [];


    payloads.reduce(function (res, value) {
      if (!res[value.location.name]) {
        res[value.location.name] = { name: value.location.name, money: 0 };
        chartOne.push(res[value.location.name])
      }
      res[value.location.name].money += value.money;
      return res;
    }, {});


    //Trabajador con dinero acumulado

    var chartTwo = [];

    payloads.reduce(function (res, value) {
      if (!res[value.employee.name]) {
        res[value.employee.name] = { name: value.employee.name, money: 0 };
        chartTwo.push(res[value.employee.name])
      }
      res[value.employee.name].money += value.money;
      return res;
    }, {});

    //Los 10 clientes que más gastan

    var chartThree = [];

    payloads.filter(x => x.is_scanned == true).reduce(function (res, value) {
      if (!res[value.client.user_account.email]) {
        res[value.client.user_account.email] = { name: value.client.name + " " + value.client.surname, money: 0 };
        chartThree.push(res[value.client.user_account.email])
      }
      res[value.client.user_account.email].money += value.money;
      return res;
    }, {});


    chartOne.sort((a, b) => (b.money > a.money) ? 1 : ((a.money > b.money) ? -1 : 0));

    chartTwo.sort((a, b) => (b.money > a.money) ? 1 : ((a.money > b.money) ? -1 : 0));

    chartThree.sort((a, b) => (b.money > a.money) ? 1 : ((a.money > b.money) ? -1 : 0));


    var chartLocationDineroAcumulado = [];
    var chartEmployeeCanjeaDinero = [];
    var chartTOP10Clientes = [];

    for (let i = 0; i < chartOne.length; i++) {


      chartLocationDineroAcumulado[i] = {
        y: chartOne[i].money,
        label: chartOne[i].name

      };

    }

    for (let j = 0; j < chartTwo.length; j++) {


      chartEmployeeCanjeaDinero[j] = {
        y: chartTwo[j].money,
        label: chartTwo[j].name

      };

    }

    for (let x = 0; x < 10; x++) {

      if (chartThree[x]) {


        chartTOP10Clientes[x] = {
          y: chartThree[x].money,
          label: chartThree[x].name

        };

      } else {
        break;
      }
    }


    this.chartOneColumns = new CanvasJS.Chart("chartPieContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.titleFirstChart,
        fontFamily: "arial",
        fontWeight: "bold"
      },
      data: [{
        type: "pie",
        dataPoints: chartLocationDineroAcumulado
      }]
    });


    this.chartTwoColumns = new CanvasJS.Chart("chartTwoColumnsContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.titleSecondChart,
        fontFamily: "arial",
        fontWeight: "bold"
      },
      data: [{
        type: "column",
        dataPoints: chartEmployeeCanjeaDinero
      }]
    });

    this.chartThreePyramid = new CanvasJS.Chart("chartPieClientsContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.titleThirdChart,
        fontFamily: "arial",
        fontWeight: "bold"
      },
      data: [{
        type: "pie",
        dataPoints: chartTOP10Clientes
      }]
    });

    if (payloads.length != 0) {

      this.chartOneColumns.render();
      this.chartTwoColumns.render();
      this.chartThreePyramid.render();

    }





  }
  updateTitlesOnInit(): void {

    if (this.translate.currentLang == "en") {
      this.titleFirstChart = "Accumulated money per location";
      this.titleSecondChart = "Accumulated money per employee";
      this.titleThirdChart = "TOP 10 spending customers";
    } else {
      this.titleFirstChart = "Dinero acumulado por establecimiento";
      this.titleSecondChart = "Dinero acumulado por trabajador";
      this.titleThirdChart = "TOP 10 clientes que más gastan";

    }
  }


  updateTitles(): void {

    this.updateTitlesOnInit();

    if (this.chartOneColumns != null) {
      this.chartOneColumns.title.set("text", this.titleFirstChart);
      this.chartTwoColumns.title.set("text", this.titleSecondChart);
      this.chartThreePyramid.title.set("text", this.titleThirdChart);
    }

  }

  showLocation(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["location-show"]);
  }

}
