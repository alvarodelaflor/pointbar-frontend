import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ElementRef,
  ViewChild
} from "@angular/core";
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
  ValidatorFn,
  AbstractControl,
  ValidationErrors
} from "@angular/forms";
import { Router } from "@angular/router";
import { ChallengeService } from "../services/challenge.service";
import { ThrowStmt } from "@angular/compiler";
import { Location, LocationService } from "../services/location.service";
import { UserService } from "../services/user.service";
import { SubscriptionService } from "../services/subscription.service";
import { UploadService } from '../services/upload.service';
import { ModalChallengeCreateComponent } from '../modals/modal-challenge-create/modal-challenge-create.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: "app-challenge-create",
  templateUrl: "./challenge-create.component.html",
  styleUrls: ["./challenge-create.component.css"]
})
export class ChallengeCreateComponent implements OnInit {
  @ViewChild("myFile")
  myFileVariable: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private challengeService: ChallengeService,
    private cd: ChangeDetectorRef,
    private locationService: LocationService,
    private subscriptionService: SubscriptionService,
    private uploadService: UploadService,
    private modalService: NgbModal
  ) {}

  createForm: FormGroup;
  isSubmitted: boolean = false;

  // Normal: "F", "C", "D", "IM"
  // VIP: "AM", "CM"
  types = ["F", "C", "D", "IM"];

  is_vip = false;

  selected_type: any;

  location_id: any;

  ngOnInit(): void {
    var ua = UserService.getuser_account();
    if (!ua || ua.authority != "BAR") this.router.navigate(["home"]);
    this.location_id = localStorage["location_id"];
    if (!this.location_id) this.router.navigate(["home"]);

    this.subscriptionService.getSubscriptions().subscribe(
      data => {
        const subscription = data.filter(
          e => e.location.id == this.location_id
        )[0];
        if (subscription.membership.membership_type == "VIP"){
          this.types.push("AM");
          this.types.push("CM");
        }
      },
      err => this.router.navigate(["home"])
    );

    this.createForm = this.formBuilder.group(
      {
        title: ["", [Validators.required ,Validators.maxLength(60)]],
        description: ["", [Validators.required ,Validators.maxLength(250)]],
        number_points: [0, [Validators.required, Validators.min(0), Validators.max(1000000)]],
        photo: [""],
        start_date: ["", [Validators.required, this.dateAfter]],
        expiration_date: ["", [Validators.required, this.dateAfter]],
        money_spent: [0, [Validators.required, Validators.min(0), Validators.max(1000000)]],
        number: [1, [Validators.required, Validators.min(1), Validators.max(1000000)]],
        tipe: ["", Validators.required],
        location: this.formBuilder.group({
          id: [this.location_id, Validators.required]
        })
      },
      { validators: this.dateRange }
    );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    this.challengeService.createChallenge(this.createForm.value).subscribe(
      data => {
        window.localStorage.setItem("locationId", this.location_id);
        this.router.navigate(["challenge-list"]);
      },
      error => {
        const errors = error.error;
        let msg = "Back-end errors";
        for (let key in errors) {
          let value = errors[key];
          msg += "\n" + key + " -> " + value;
        }
        alert(msg);
      }
    );
  }

  hasErrors() {
    let hasErrors: boolean = false;
    if (this.createForm.errors != null) {
      hasErrors = true;
    }
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    Object.keys(this.locationControls).forEach(key => {
      const controlErrors: ValidationErrors = this.createForm
        .get("location")
        .get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  get formControls() {
    return this.createForm.controls;
  }

  get locationControls() {
    var location = <FormGroup>this.createForm.get("location");
    return location ? location.controls : null;
  }

  get photo() {
    return this.createForm.value.photo;
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let valid = true;
      if (file.size > 2000000){ //2MB
        valid = false;
        let control = this.createForm.get("photo");
        control.setErrors({ size: true });
      }else{
        this.formControls["photo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.myFileVariable.nativeElement.value = "";
        this.createForm.value.photo = null;
        return;
      }
      this.createForm.patchValue({
        photo: "../../assets/img/loading.gif",
      });
      this.cd.markForCheck();
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        this.createForm.patchValue({
          photo: data.Location,
        });
        this.cd.markForCheck();
      });
    }
  }

  dateAfter(formControl: FormControl) {
    const value = formControl.value;
    const date = new Date(value);
    const curr_date = new Date();
    curr_date.setHours(0, 0, 0, 0);
    if (curr_date > date) {
      return {
        after: true
      };
    } else {
      return null;
    }
  }

  dateRange(formGroup: FormGroup) {
    const start_date_input = formGroup.get("start_date").value;
    const expiration_date_input = formGroup.get("expiration_date").value;
    const start_date = new Date(start_date_input);
    const expiration_date = new Date(expiration_date_input);
    if (start_date >= expiration_date) {
      return {
        date_range: true
      };
    } else {
      return null;
    }
  }

  onChange(value: String) {
    // We need to do this to avoid validation errors
    this.createForm.get("number").setValue(1); // Reset consumptions or days
    this.createForm.get("money_spent").setValue(0); // Reset money
  }

  openModal() {
    const modalRef = this.modalService.open(ModalChallengeCreateComponent);
    modalRef.componentInstance.challenge = this.createForm.value;
    modalRef.result.then((result) => {
      // Close
    }, (reason) => {
      // Save
      if (reason == "save")
        this.onSubmit();
    });
  }
}
