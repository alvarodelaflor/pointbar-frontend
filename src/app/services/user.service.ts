import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpBackend } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Router } from '@angular/router';


@Injectable({
  providedIn: "root"
})
export class UserService {
  apiUrl = environment.apiUrl;

  httpOptions: any;

  // error messages received from the login attempt
  public errors: any = [];

  constructor(private http: HttpClient, private handler: HttpBackend,  private router: Router) {
    if (!localStorage["access"]) {
      localStorage["access"] = "";
    }
    if (!localStorage["refresh"]) {
      localStorage["refresh"] = "";
    }
    this.httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    };
  }

  // Uses http.post() to get an auth token from djangorestframework-jwt endpoint
  public login(user) {
    return this.http
      .post(this.apiUrl + "/api/token/", user, this.httpOptions)

  }

  // Refreshes the JWT token, to extend the time the user is logged in
  public refreshToken() {
    let http = new HttpClient(this.handler); // Prevents interceptor
    return http
      .post(this.apiUrl + "/api/token/refresh/", {
        refresh: localStorage["refresh"]
      })
      .toPromise()
      .then(
        data => {
          this.updateData(data);
        },
        err => {
          this.errors = err["error"];
        }
      );
  }

  public logout() {
    localStorage["access"] = "";
    localStorage["refresh"] = "";
  }

  public updateData(data) {
    localStorage["access"] = data["access"];
    if (data["refresh"]) localStorage["refresh"] = data["refresh"];
  }

  public static getuser_account() {
    var access = localStorage["access"];
    if (!access) return null;
    const token_parts = access.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    var user_account = {
      user_id: token_decoded["user_id"],
      username: token_decoded["username"],
      email: token_decoded["email"],
      authority: token_decoded["authority"]
    };
    return user_account;
  }

  public hasAuthority(authority) {
    var ua = UserService.getuser_account();
    if (!ua) return false;
    return ua.authority == authority;
  }

  public isAuthenticated() {
    var ua = UserService.getuser_account();
    return ua != undefined;
  }

  public static checkLogged() {
    var ua = UserService.getuser_account();
    return ua != undefined;
  }

  public static getHttpOptions() {
    return {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage["access"]
      })
    };
  }
}
