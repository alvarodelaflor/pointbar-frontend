import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError, empty, from } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Router } from "@angular/router";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private router: Router, private userService: UserService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.handle(req, next));
  }

  async handle(req: HttpRequest<any>, next: HttpHandler) {
    const token: string = localStorage["access"];
    let request = req;
    if (token) {
      await this.userService.refreshToken();

      request = req.clone({
        setHeaders: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage["access"]
        }
      });
    }
    return next
      .handle(request)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) this.router.navigateByUrl("login");
          if (err.status === 500) this.router.navigateByUrl("home");
          return throwError(err);
        })
      )
      .toPromise();
  }
}
