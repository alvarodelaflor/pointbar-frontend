import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Employee } from './employee.service';
import { Client } from './client.service';
import { Sale } from './sale.service';

export interface Exchange {
  id: number,
  points: number,
  qr_code: string,
  create_moment: Date,
  scanned_moment: Date,
  is_scanned: boolean,
  client: Client,
  employee: Employee,
  sale: Sale,
}

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getExchangeByCode(code: number): Observable<Exchange[]> {
    const url = this.apiUrl + '/api/exchange/code/?number=' + code;
    return this.httpClient.get<Exchange[]>(url);
  }

  getExchangesFilterBar(query: string): Observable<Exchange[]> {
    const url = this.apiUrl + query;
    return this.httpClient.get<Exchange[]>(url);
  }

  updateExchangeandPoints(code: number): Observable<Sale[]> {
    const url = this.apiUrl + '/api/exchange-point/?code=' + code;
    return this.httpClient.get<any>(url);
  }

  createExchangeBySaleId(id: number): Observable<Exchange[]> {
    const url = this.apiUrl + '/api/exchange/sale/?sale_id=' + id;
    return this.httpClient.get<any>(url);
  }
  getPaginatedExchangesFilterBar(APIurl:string, query: string): Observable<any> {
    const url = APIurl + query;
    return this.httpClient.get<any>(url);
  }
}
