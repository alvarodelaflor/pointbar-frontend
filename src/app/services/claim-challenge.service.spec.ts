import { TestBed } from '@angular/core/testing';

import { ClaimChallengeService } from './claim-challenge.service';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ClaimChallengeService', () => {
  let service: ClaimChallengeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      schemas: [NO_ERRORS_SCHEMA]
    });
    service = TestBed.inject(ClaimChallengeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
