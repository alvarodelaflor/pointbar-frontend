import { TestBed } from '@angular/core/testing';

import { CompletedChallengesService } from './completed-challenges.service';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CompletedChallengesService', () => {
  let service: CompletedChallengesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      schemas: [NO_ERRORS_SCHEMA]
    });
    service = TestBed.inject(CompletedChallengesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
