import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Challenge } from './challenge.service';

export interface CompletedChallenge {
  last_challenge_moment: Date;
  count: number;
  challenge: Challenge;
  client: number;
}

@Injectable({
  providedIn: "root"
})
export class CompletedChallengesService {
  apiUrl = environment.apiUrl + "/api";

  constructor(private httpClient: HttpClient) {}

  getCompletedChallenges(): Observable<CompletedChallenge[]> {
    const url = this.apiUrl + "/mycompletedchallenges";
    return this.httpClient.get<CompletedChallenge[]>(url);
  }
}
