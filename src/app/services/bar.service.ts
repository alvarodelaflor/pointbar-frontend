import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs'
import { environment } from '../../environments/environment';
import { UserService } from '../services/user.service';


export interface user_account{
  username: string,
  password1: string,
  password2: string,
  email: string,
  user_id: string
}

export interface Bar {
  name: string,
  phone: string,
  terms_and_conditions: string,
  user_account: user_account,
}

export interface BarUpdate {
  id: number,
  name: string,
  terms_and_conditions: string,
  phone: string,
}

export interface BarRegistration {
  name: string,
  phone: string,
}

@Injectable({
  providedIn: 'root'
})
export class BarService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getBars(): Observable<Bar[]> {
    const url = this.apiUrl + '/bars'
    return this.httpClient.get<Bar[]>(url);
  }

  getBar(): Observable<Bar[]> {
    const url = this.apiUrl + '/api/bar/get/';
    return this.httpClient.get<Bar[]>(url);
  }

  createBar(bar: Bar): Observable<any> {
    const url = this.apiUrl + '/api/bar/create/';
    return this.httpClient.post<any>(url, bar);
  }

  updateBar(bar: BarUpdate): Observable<any> {
    const url = this.apiUrl + '/api/bar/update/';
    return this.httpClient.put<any>(url, bar);
  }

  deleteBarAnon(bar: BarUpdate): Observable<any> {
    const url = this.apiUrl + '/api/bar/delete/';
    return this.httpClient.post<any>(url, bar);
  }

  deleteBar(id: number) {
    const url = this.apiUrl + '/bars/' + id;
    return this.httpClient.delete(url);
  }

  export(id: number): Observable<any> {
    const url = this.apiUrl + '/api/personalInfo/' + id;
    return this.httpClient.get<any>(url);
  }
}
