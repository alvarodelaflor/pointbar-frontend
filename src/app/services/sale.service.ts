import { Injectable } from '@angular/core';
import { UserService } from "./user.service";
import { HttpClient, HttpHeaders, HttpEvent, HttpResponse} from "@angular/common/http";
import { Observable} from 'rxjs';
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Location } from './location.service';

export interface Sale {
  id: number,
  title: string,
  description: boolean,
  number_points: number,
  photo: string,
  is_active:boolean,
  start_date:Date,
  expiration_date:Date,
  location: Location
}

export interface SaleForScanned {
  id: number,
  title: string,
  description: string,
  number_points: number,
  photo: string,
  is_active:boolean,
  start_date:Date,
  expiration_date:Date,
  location: Location
}

export interface SaleRegistration {
  title: string,
  description: boolean,
  number_points: number,
  photo: string,
  is_active:boolean,
  start_date:Date,
  expiration_date:Date,
  location:string
}

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  apiUrl = environment.apiUrl;
  httpOptions: any;

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {

  }

  getSales(id: number): Observable<Sale[]> {
    const url = this.apiUrl + '/api/salesperlocation/'+ id
    return this.httpClient.get<Sale[]>(url);
  }

  getSalesLocation(id: number): Observable<Sale[]> {
    const url = this.apiUrl + '/api/salesperlocation/'+ id
    return this.httpClient.get<Sale[]>(url);
  }

  getSaleById(id: string): Observable<Sale> {
    const url = this.apiUrl + '/api/sales/' + id + '/';
    return this.httpClient.get<Sale>(url);
  }

  createSale(sale: Sale): Observable<any> {
    const url = this.apiUrl + '/api/sales';
    return this.httpClient.post<any>(url, sale);
  }

  updateSale(sale: Sale): Observable<any> {
    const url = this.apiUrl + '/api/sales/' + sale.id + "/";
    return this.httpClient.put<any>(url, sale);
  }

  deleteSale(sale: Sale) {
    const url = this.apiUrl + '/api/sales/' + sale.id + '/';
    return this.httpClient.delete(url);
  }

  getPaginatedSales(APIUrl: string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }
}
