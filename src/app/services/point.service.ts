import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Location } from './location.service';
import { Client } from './client.service';

export interface Point {
  id: number,
  quantity: number,
  client: Client,
  location: Location,
}

@Injectable({
  providedIn: 'root'
})
export class PointService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getPointByLoggedClientAndLocationId(id_location: number): Observable<Point[]> {
    const url = this.apiUrl + '/api/point/location/' + id_location;
    return this.httpClient.get<Point[]>(url);
  }

  getPaginatedPoints(APIUrl:string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }


}
