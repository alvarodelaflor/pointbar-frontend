import { Injectable } from '@angular/core';
import { Challenge } from './challenge.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClaimChallengeService {

  apiUrl = environment.apiUrl + "/api";

  constructor(private httpClient: HttpClient) { }

  claimChallenge(challenge: Challenge): Observable<any> {
    const url = this.apiUrl + '/claim_challenge/' + challenge.id + "/";
    return this.httpClient.get<any>(url);
  }
}
