import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

interface ChallengeCreate {
  title: string;
  description: string;
  number_points: number;
  photo: string;
  expiration_date: Date;
  money_spent: number;
  number: number;
  tipe: string;
}

export interface Challenge {
  id: string;
  title: string;
  description: string;
  number_points: number;
  photo: string;
  start_date: Date;
  expiration_date: Date;
  is_active: boolean;
  money_spent: number;
  number: number;
  tipe: string;
  location: {
    id: string;
  };
}

@Injectable({
  providedIn: "root"
})
export class ChallengeService {
  apiUrl = environment.apiUrl + "/api";

  constructor(private httpClient: HttpClient) {}

  createChallenge(challenge: ChallengeCreate): Observable<any> {
    const url = this.apiUrl + "/challenges";
    return this.httpClient.post<any>(url, challenge);
  }

  getChallengesByLocation(locationId: string): Observable<Challenge[]> {
    const url = this.apiUrl + "/challengesperlocation/" + locationId;
    return this.httpClient.get<Challenge[]>(url);
  }

  getChallenge(id: string): Observable<Challenge[]> {
    const url = this.apiUrl + "/challenge/" + id;
    return this.httpClient.get<Challenge[]>(url);
  }
  
  getPaginatedChallenges(APIUrl:string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }
}
