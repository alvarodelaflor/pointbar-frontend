import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Url } from 'url';

export interface Membership {
  id: number,
  titleS: string,
  descriptionS: string,
  titleE: string,
  descriptionE: string,
  price: number,
  membership_type: string,
  subscriptionId:string
}

@Injectable({
  providedIn: 'root'
})
export class MembershipService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getMemberships(): Observable<Membership[]> {
    const url = this.apiUrl + '/api/membership'
    return this.httpClient.get<Membership[]>(url);
  }

  getMembershipById(id: number): Observable<Membership> {
    const url = this.apiUrl + '/api/membership/' + id;
    return this.httpClient.get<Membership>(url);
  }

  createMembership(membership: Membership): Observable<any> {
    const url = this.apiUrl + '/membership/';
    return this.httpClient.post<any>(url, membership);
  }

  updateMembership(membership: Membership): Observable<any> {
    const url = this.apiUrl + '/membership/' + membership.id + '/';
    return this.httpClient.put<any>(url, membership);
  }

  deleteMembership(id: number) {
    const url = this.apiUrl + '/membership/' + id;
    return this.httpClient.delete(url);
  }

}
