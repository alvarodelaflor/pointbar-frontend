import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Employee } from './employee.service';
import { Client } from './client.service';
import { Location } from './location.service';

export interface Payload {
  id: number,
  points: number,
  qr_code: string,
  money: number,
  create_moment: Date,
  scanned_moment: Date,
  is_scanned: boolean,
  employee: Employee,
  location: Location,
  client: Client
}

@Injectable({
  providedIn: 'root'
})
export class PayloadService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getPayloads(): Observable<Payload[]> {
    const url = this.apiUrl + '/api/payload/';
    return this.httpClient.get<Payload[]>(url);
  }

  getPayloadsFilterBar(query: string): Observable<Payload[]> {
    const url = this.apiUrl + query;
    return this.httpClient.get<Payload[]>(url);
  }

  getPayloadByCode(code: number): Observable<Payload[]> {
    const url = this.apiUrl + '/api/payload/code/?number=' + code;
    return this.httpClient.get<Payload[]>(url);
  }

  createPayload(payload: Payload): Observable<any> {
    const url = this.apiUrl + '/api/payload/';
    return this.httpClient.post<any>(url, payload);
  }


  updatePayloadandPoints(code: number): Observable<Payload[]> {
    const url = this.apiUrl + '/api/payload-point/?code=' + code;
    return this.httpClient.get<any>(url);
  }

  sendPoints(username: string, money: number): Observable<Payload[]> {
    const url = this.apiUrl + '/api/payload/send/?username=' + username + "&money=" + money;
    return this.httpClient.post<any>(url, url);
  }

  getPaginatedPayloadsFilterBar(APIurl:string, query: string): Observable<any> {
    const url = APIurl + query;
    return this.httpClient.get<any>(url);
  }
}
