import { Injectable } from '@angular/core';
import { UserService } from "./user.service";
import { HttpClient, HttpHeaders, HttpEvent, HttpResponse} from "@angular/common/http";
import { Observable} from 'rxjs'
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';

export interface Notification  {
  id: number,
  verb: string,
  timestamp: string,
}

@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  apiUrl = environment.apiUrl;
  httpOptions: any;

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {

  }

  getNotifications(){
    const url = this.apiUrl + '/api/unread_list/'
    return this.httpClient.get(url);
  }

  getNotificationsRead() {
    const url = this.apiUrl + '/api/all_list/'
    return this.httpClient.get(url);
  }

  markAsRead(id: number) {
    const url = this.apiUrl + '/api/notification/mark-as-read/'
    return this.httpClient.post(url, {id: id});
  }

  delete(id: number) {
    const url = this.apiUrl + '/api/notification/delete/'
    return this.httpClient.post(url, {id: id});
  }
  getPaginatedNotifications(APIUrl:string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }
}
