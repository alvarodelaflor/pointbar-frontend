import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Url } from 'url';

@Injectable({
  providedIn: 'root'
})
export class ActivationService {

  apiUrl = environment.apiUrl + "/api";

  httpOptions: any;

  constructor(private httpClient: HttpClient) { }

  activateAccount(uid: String, token:String): Observable<boolean> {
    const url = this.apiUrl + '/account/activate/' + uid + "/" + token;
    return this.httpClient.get<boolean>(url);
  }
}
