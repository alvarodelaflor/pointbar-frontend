import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { UserService } from "./user.service";
import { Location } from "./location.service";
import { Bar } from "./bar.service"

// tslint:disable-next-line:class-name
export interface user_account {
  username: string;
  password1: string;
  password2: string;
  email: string;
}

export interface Employee {
  id: number;
  username: string;
  password: string;
  email: string;
  name: string;
  is_working: boolean;
  inscriptionDate: Date,
  isBanned: boolean,
  control: boolean,
  limit_points: number,
  granted_points: number,
  location: Location,
  bar_field: Bar,
  user_account: user_account
}

export interface EmployeeChange {
  id: number;
  name: string;
  is_working: boolean;
  granted_points: number;
  location: Location;
}
export interface EmployeeChangePost {
  is_working: boolean;
  granted_points: number;
  location: string;
}

export interface EmployeeRegistration {
  terms_and_conditions: boolean;
  name: string;
  is_working: boolean;
  location: string;
}


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  apiUrl = environment.apiUrl;

  httpOptions: any;

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) { }

  getEmployees(): Observable<HttpEvent<Employee[]>> {
    const url = this.apiUrl + '/api/employees/';
    return this.httpClient.get<Employee[]>(url, this.httpOptions);
  }

  getEmployees2(): Observable<EmployeeChange[]> {
    const url = this.apiUrl + '/api/employee/myemployee';
    return this.httpClient.get<EmployeeChange[]>(url);
  }

  getEmployee(): Observable<Employee[]> {
    const url = this.apiUrl + '/api/employee/get/';
    return this.httpClient.get<Employee[]>(url);
  }

  getEmployeeById(id: number): Observable<Employee> {
    const url = this.apiUrl + '/api/employee/' + id + '/edit';
    return this.httpClient.get<Employee>(url);
  }

  createEmployee(employee: EmployeeRegistration): Observable<any> {
    this.httpOptions = UserService.getHttpOptions();
    const url = this.apiUrl + '/api/employee/create/';
    return this.httpClient.post<any>(url, employee, this.httpOptions);
  }

  getEmployeeLogged(): Observable<any> {
    const url = this.apiUrl + '/api/employee/user/';
    return this.httpClient.get<Employee[]>(url);
  }

  changeEmployeeStatus(employee: EmployeeChange): Observable<any> {
    const employeeChangePost: EmployeeChangePost = {
      is_working: !employee.is_working,
      granted_points: employee.granted_points,
      location: employee.location.id.toString()
    };
    const url = this.apiUrl + '/api/employee/update/' + employee.id + '/';
    return this.httpClient.put<any>(url, employeeChangePost);
  }

  resetGrantedPoints(employee: EmployeeChange): Observable<any> {
    const employeeChangePost: EmployeeChangePost = {
      is_working: employee.is_working,
      granted_points: 0,
      location: employee.location.id.toString()
    };

    const url = this.apiUrl + '/api/employee/update/' + employee.id + '/';
    return this.httpClient.put<any>(url, employeeChangePost);
  }

  changeEmployeeLocation(employee: EmployeeChange, location: string): Observable<any> {
    const employeeChangePost: EmployeeChangePost = {
      is_working: employee.is_working,
      granted_points: 0,
      location
    };
    const url = this.apiUrl + '/api/employee/update/' + employee.id + '/';
    return this.httpClient.put<any>(url, employeeChangePost);
  }

  updateEmployee(employee: Employee): Observable<any> {
    const url = this.apiUrl + '/api/employee/' + employee.id + '/edit';
    return this.httpClient.put<any>(url, employee);
  }

  updateOwnEmployee(employee: Employee): Observable<any> {
    const url = this.apiUrl + "/api/employee/own/update/";
    return this.httpClient.put<any>(url, employee);
  }

  getPaginatedEmployees(APIUrl: string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }

  deleteEmployeeAnon(employee: EmployeeChange): Observable<any> {
    const url = this.apiUrl + '/api/employee/delete/';
    return this.httpClient.get<any>(url);
  }

  deleteEmployeeBarAnon(employee: EmployeeChange, id: number): Observable<any> {
    const url = this.apiUrl + '/api/employee/bar/delete?id=' + id;
    return this.httpClient.get<any>(url);
  }

  export(id: number): Observable<any> {
    const url = this.apiUrl + '/api/personalInfo/' + id;
    return this.httpClient.get<any>(url);
  }

}
