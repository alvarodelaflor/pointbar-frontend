import { Injectable } from '@angular/core';
import { UserService } from "./user.service";
import { HttpClient, HttpHeaders, HttpEvent, HttpResponse} from "@angular/common/http";
import { Observable} from 'rxjs'
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Membership } from './membership.service';
import { Location } from './location.service';

export interface Subscription {
  id: string,
  expiration_date: Date,
  renovate: boolean,
  is_active: boolean,
  location: Location,
  membership: Membership
}

export interface SubscriptionRegistration {
  renovate: boolean,
  location: string,
  membership: string
}

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  apiUrl = environment.apiUrl;
  httpOptions: any;

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {

  }

  getAllSubscriptionsNormal(): Observable<Subscription[]> {
    const url = this.apiUrl + '/api/subscription/normal'
    return this.httpClient.get<Subscription[]>(url);
  }

  getAllSubscriptionsVIP(): Observable<Subscription[]> {
    const url = this.apiUrl + '/api/subscription/vip'
    return this.httpClient.get<Subscription[]>(url);
  }

  getSubscriptions(): Observable<Subscription[]> {
    const url = this.apiUrl + '/api/subscription/bar'
    return this.httpClient.get<Subscription[]>(url);
  }

  getSubscriptionById(id: string): Observable<Subscription> {
    const url = this.apiUrl + '/api/subscription/' + id;
    return this.httpClient.get<Subscription>(url);
  }

  createSubscription(subscription): Observable<any> {
    const url = this.apiUrl + '/api/subscription/';
    return this.httpClient.post<any>(url, subscription);
  }

  updateSubscription(subscription: Subscription): Observable<any> {
    const url = this.apiUrl + '/subscription/update' + subscription.id + '/';
    return this.httpClient.put<any>(url, subscription);
  }

  deleteSubscription(subscriptionId) {
    const url = this.apiUrl + '/api/subscription/cancel';
    return this.httpClient.post<any>(url, {id:subscriptionId});
  }

  activateSubscription(subscription: Subscription) {
    const url = this.apiUrl + '/api/subscription/' + subscription.id + '/';
    subscription.renovate = true
    return this.httpClient.put<any>(url, subscription);
  }

  getVipSubscriptionForLogggedBar(): Observable<Subscription[]> {
    const url = this.apiUrl + '/api/subscription/bar/vip/';
    return this.httpClient.get<Subscription[]>(url);
  }
  getPaginatedSubscriptions(APIUrl: string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }
}
