import { TestBed } from '@angular/core/testing';

import { PayloadService } from './payload.service';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PayloadService', () => {
  let service: PayloadService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      schemas: [NO_ERRORS_SCHEMA]
    });
    service = TestBed.inject(PayloadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
