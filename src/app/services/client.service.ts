import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../../environments/environment';

export interface user_account{
  username: string,
  password1: string,
  password2: string,
  email: string,
}

export interface Client{
  name: string,
  phone: string,
  terms_and_conditions: string,
}

export interface ClientRegistration{
  terms_and_conditions: boolean
  name: string,
  surname: string,
  phone: string,
  photo: string,
  birth_date: Date,
}

export interface ClientEdit{
  id: number,
  name: string,
  surname: string,
  phone: string,
  photo: string,
  birth_date: Date
}

export interface ClientPayload{
  user_account: user_account,
  id: number,
  name: string,
  surname: string,
  phone: string,
  photo: string,
  birth_date: Date
}

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllClients(url: string, query: string): Observable<any> {
    const resquest = url + query;
    return this.httpClient.get<any>(resquest);
  }

  getClient(): Observable<Client[]> {
    const url = this.apiUrl + '/api/client/get/';
    return this.httpClient.get<Client[]>(url);
  }

  createClient(clientRegistration: ClientRegistration): Observable<any> {
    const url = this.apiUrl + '/api/client/create/';
    return this.httpClient.post<any>(url, clientRegistration);
  }

  updateClient(client: ClientEdit): Observable<any> {
    const url = this.apiUrl + '/api/client/update/';
    return this.httpClient.put<any>(url, client);
  }

  getClientByUserAccountId(id: number): Observable<any> {
    const url = this.apiUrl + '/api/client/user/?id=' + id;
    return this.httpClient.get<Client[]>(url);
  }


  deleteBarAnon(client: ClientEdit): Observable<any> {
    const url = this.apiUrl + '/api/client/delete/';
    return this.httpClient.post<any>(url, client);
  }

  export(id: number): Observable<any> {
    const url = this.apiUrl + '/api/personalInfo/' + id;
    return this.httpClient.get<any>(url);
  }
}
