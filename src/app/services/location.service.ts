import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Url } from 'url';
import { UserService } from "./user.service";

export interface Location {
  id: number,
  name: string,
  description: string,
  email: string,
  website: Url,
  logo: string,
  photo: string[],
  latitude: string,
  longitude: string,
  street: string,
  city: string,
  state: string,
  postal_code: string,
  country: string,
  is_active: boolean
}

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  apiUrl = environment.apiUrl + "/api";

  httpOptions: any;

  constructor(private httpClient: HttpClient) { }

  getLocations(): Observable<Location[]> {
    const url = this.apiUrl + '/location'
    return this.httpClient.get<Location[]>(url);
  }

  getLocationsWithoutSubscriptions(): Observable<Location[]> {
    const url = this.apiUrl + '/location/bar/nosubscription'
    return this.httpClient.get<Location[]>(url);
  }

  getPaginatedLocations(APIUrl:string): Observable<any> {
    return this.httpClient.get<any>(APIUrl);
  }


  getLocationsOfMap(): Observable<Location[]> {
    const url = this.apiUrl + '/location/list'
    return this.httpClient.get<Location[]>(url);
  }

  getLocationById(id: number): Observable<Location> {
    const url = this.apiUrl + '/location/' + id;
    return this.httpClient.get<Location>(url);
  }

  getLocationById2(id: number): Observable<any> {
    const url = this.apiUrl + '/location/' + id;
    return this.httpClient.get<any>(url);
  }

  createLocation(location: Location): Observable<any> {
    const url = this.apiUrl + '/location/';
    return this.httpClient.post<any>(url, location);
  }

  updateLocation(location: Location): Observable<any> {
    const url = this.apiUrl + '/location/' + location.id + '/';
    return this.httpClient.put<any>(url, location);
  }

  deleteLocation(id: number) {
    const url = this.apiUrl + '/location/' + id + '/delete';
    return this.httpClient.delete(url);
  }

}
