import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LocationCreateComponent } from "./location-create/location-create.component";
import { HomeComponent } from "./home/home.component";
import { ClientRegisterComponent } from "./client-register/client-register.component";
import { ClientEditComponent } from "./client-edit/client-edit.component";
import { BarRegisterComponent } from "./bar-register/bar-register.component";
import { BarEditComponent } from "./bar-edit/bar-edit.component";
import { EmployeeRegisterComponent } from "./employee-register/employee-register.component";
import { EmployeeEditComponent } from "./employee-edit/employee-edit.component";
import { LoginComponent } from "./login/login.component";
import { SaleCreateComponent } from "./sale-create/sale-create.component";
import { SaleEditComponent } from "./sale-edit/sale-edit.component";
import { SaleListAuthorizedComponent } from "./sale-list-authorized/sale-list-authorized.component";
import { SubscriptionCreateComponent } from "./subscription-create/subscription-create.component";
import { SubscriptionListComponent } from "./subscription-list/subscription-list.component";
import { SubscriptionShowComponent } from "./subscription-show/subscription-show.component";
import { PayloadCreateComponent } from './payload-create/payload-create.component';
import { PayloadShowComponent } from './payload-show/payload-show.component';
import { PayloadScanComponent } from './payload-scan/payload-scan.component';
import { PayloadListComponent } from './payload-list/payload-list.component';
import { PointListComponent } from "./point-list/point-list.component";
import { ExchangeShowComponent } from './exchange-show/exchange-show.component';
import { ExchangeScanComponent } from './exchange-scan/exchange-scan.component';
import { ExchangeListComponent } from './exchange-list/exchange-list.component';
import { ChallengeCreateComponent } from "./challenge-create/challenge-create.component";
import { MapComponent } from "./map/map.component";
import { LocationListComponent } from "./location-list/location-list.component";
import { LocationEditComponent } from "./location-edit/location-edit.component";
import { ChallengeListComponent } from "./challenge-list/challenge-list.component";
import { LocationShowComponent } from "./location-show/location-show.component";
import { TermsAndConditionsComponent } from "./terms-and-conditions/terms-and-conditions.component";
import { AboutComponent } from "./about/about.component";
import { EmployeeListComponent } from "./employee-list/employee-list.component";
import { CompletedChallengesListComponent } from "./completed-challenges-list/completed-challenges-list.component";
import { PaymentSuccessComponent } from "./payment-success/payment-success.component";
import { LocationsNfoundComponent } from "./locations-nfound/locations-nfound.component";
import { NotificationListComponent } from './notification-list/notification-list.component';
import { CompletedChallengeShowComponent } from './completed-challenge-show/completed-challenge-show.component';
import { ClientAdvertisementComponent } from './client-advertisement/client-advertisement.component';
import { ActivationComponent } from './activation/activation.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "create-location", component: LocationCreateComponent },
  { path: "client-register", component: ClientRegisterComponent },
  { path: "client-edit", component: ClientEditComponent },
  { path: "bar-register", component: BarRegisterComponent },
  { path: "bar-edit", component: BarEditComponent },
  { path: "employee-register", component: EmployeeRegisterComponent },
  { path: "login", component: LoginComponent },
  { path: "create-sale", component: SaleCreateComponent },
  { path: "list-sale", component: SaleListAuthorizedComponent },
  { path: "edit-sale", component: SaleEditComponent },
  { path: "map", component: MapComponent },
  { path: "create-payload", component: PayloadCreateComponent },
  { path: "show-payload", component: PayloadShowComponent },
  { path: "scan-payload", component: PayloadScanComponent },
  { path: 'list-payload', component: PayloadListComponent },
  { path: 'list-exchange', component: ExchangeListComponent },
  { path: 'list-point', component: PointListComponent },
  { path: "show-exchange", component: ExchangeShowComponent },
  { path: "scan-exchange", component: ExchangeScanComponent },
  { path: "create-subscription", component: SubscriptionCreateComponent },
  { path: "list-subscriptions", component: SubscriptionListComponent },
  { path: "show-subscription", component: SubscriptionShowComponent },
  { path: "location-list", component: LocationListComponent },
  { path: "location-edit", component: LocationEditComponent },
  { path: "create-challenge", component: ChallengeCreateComponent },
  { path: "challenge-list", component: ChallengeListComponent },
  { path: "location-show", component: LocationShowComponent },
  { path: "terms-and-conditions", component: TermsAndConditionsComponent },
  { path: "about", component: AboutComponent },
  { path: "employee-list", component: EmployeeListComponent },
  { path: "employee-edit", component: EmployeeEditComponent },
  {
    path: "completed-challenges-list",
    component: CompletedChallengesListComponent
  },
  { path: "payment-success", component: PaymentSuccessComponent },
  { path: "locations-nfound", component: LocationsNfoundComponent },
  { path: 'notification-list', component: NotificationListComponent },
  { path: 'completed-challenge-show', component: CompletedChallengeShowComponent },
  { path: 'client-advertisement', component: ClientAdvertisementComponent},
  { path: 'activation/:uid/:token', component: ActivationComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
