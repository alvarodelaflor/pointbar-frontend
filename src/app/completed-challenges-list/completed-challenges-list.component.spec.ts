import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CompletedChallengesListComponent } from "./completed-challenges-list.component";
import { RouterModule } from "@angular/router";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { AppRoutingModule } from "../app-routing.module";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from '../app.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe("CompletedChallengesListComponent", () => {
  let component: CompletedChallengesListComponent;
  let fixture: ComponentFixture<CompletedChallengesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        AppRoutingModule,
        TranslateModule.forRoot({
          defaultLanguage: "en",
          loader: {
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [CompletedChallengesListComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedChallengesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
