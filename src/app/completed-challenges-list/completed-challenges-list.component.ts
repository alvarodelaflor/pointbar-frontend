import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/user.service";
import { Router } from "@angular/router";
import {
  CompletedChallengesService,
  CompletedChallenge
} from "../services/completed-challenges.service";

@Component({
  selector: "app-completed-challenges-list",
  templateUrl: "./completed-challenges-list.component.html",
  styleUrls: ["./completed-challenges-list.component.css"]
})
export class CompletedChallengesListComponent implements OnInit {
  constructor(
    private router: Router,
    private completedChallengesService: CompletedChallengesService
  ) {}

  completedChallenges: CompletedChallenge[] = [];

  ngOnInit(): void {
    if (
      !UserService.getuser_account() ||
      UserService.getuser_account().authority != "CLIENT"
    )
      this.router.navigate["home"];
    this.completedChallengesService.getCompletedChallenges().subscribe(
      data => {
        this.completedChallenges = data;
      },
      err => {
        this.router.navigate(["home"]);
      }
    );
  }
}
