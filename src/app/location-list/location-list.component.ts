import { Component, OnInit } from "@angular/core";
import { LocationService, Location } from "../services/location.service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-location-list",
  templateUrl: "./location-list.component.html",
  styleUrls: ["./location-list.component.css"]
})
export class LocationListComponent implements OnInit {
  locations: Location[] = [];
  locationsURL = environment.apiUrl + "/api/location/pagination";
  next: string;
  previous: string;
  isLoading: boolean = false;
  empty: boolean = false;

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private locationService: LocationService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.setLocations(this.locationsURL);
  }

  changeStatus(location: Location): void {
    if (this.translateService.currentLang == "en") {
      if (confirm("Are you sure you want to delete " + location.name + " ?")) {
        this.locationService.deleteLocation(location.id).subscribe(result => {
          this.locations = this.locations.filter(elem => {
            return elem.id !== location.id;
          });
        });
      }
    } else {
      if (
        confirm("¿Está seguro de que quiere borrar " + location.name + " ?")
      ) {
        this.locationService.deleteLocation(location.id).subscribe(result => {
          this.locations = this.locations.filter(elem => {
            return elem.id !== location.id;
          });
        });
      }
    }
  }

  editLocation(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["location-edit"]);
  }

  challengeList(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["challenge-list"]);
  }

  showLocation(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["location-show"]);
  }

  listSales(location: Location): void {
    localStorage.removeItem("locationId");
    localStorage.setItem("locationId", location.id.toString());
    this.router.navigate(["list-sale"]);
  }

  setLocations(url: string) {
    this.isLoading = true;
    this.empty = false;
    this.locationService.getPaginatedLocations(url).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
        this.spinner.hide();
      } else {
        this.next = "null";
        this.spinner.hide();
      }
      this.locations = this.locations.concat(response.results);

      if (this.locations.length <= 0) {
        this.empty = true;
      }

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, err => {
      // this.router.navigate(["home"]);
    });
  }

  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.spinner.show();
      this.setLocations(this.next);
    }
  }
}
