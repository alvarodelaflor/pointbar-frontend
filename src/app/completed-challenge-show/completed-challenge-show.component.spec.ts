import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedChallengeShowComponent } from './completed-challenge-show.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CompletedChallengeShowComponent', () => {
  let component: CompletedChallengeShowComponent;
  let fixture: ComponentFixture<CompletedChallengeShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedChallengeShowComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedChallengeShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
