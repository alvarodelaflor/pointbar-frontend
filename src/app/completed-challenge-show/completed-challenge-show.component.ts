import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-completed-challenge-show',
  templateUrl: './completed-challenge-show.component.html',
  styleUrls: ['./completed-challenge-show.component.css']
})
export class CompletedChallengeShowComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
