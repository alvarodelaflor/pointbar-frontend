import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ValidationErrors
} from "@angular/forms";
import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef
} from "@angular/core";
import { Router } from "@angular/router";
import { SaleService } from "../services/sale.service";
import { UploadService } from '../services/upload.service';

@Component({
  selector: "app-sale-edit",
  templateUrl: "./sale-edit.component.html",
  styleUrls: ["./sale-edit.component.css"]
})
export class SaleEditComponent implements OnInit {
  @ViewChild("myFile")
  myFileVariable: ElementRef;

  location: Location;
  editForm: FormGroup;
  isSubmitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private saleService: SaleService,
    private cd: ChangeDetectorRef,
    private uploadService: UploadService
  ) {}

  ngOnInit(): void {
    const urlRegex = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
    const pointsRegex = "^[0-9]{1,}";
    var locationId= +localStorage.getItem('locationId')
    this.editForm = this.formBuilder.group(
      {
        id: [""],
        title: ["", [Validators.required ,Validators.maxLength(60)]],
        description: ["", [Validators.required ,Validators.maxLength(250)]],
        number_points: [
          "",
          [Validators.required, Validators.pattern(pointsRegex)]
        ],
        photo: [""],
        start_date: ["", [Validators.required, this.dateAfter]],
        expiration_date: ["", [Validators.required, this.dateAfter]],
        is_active: ["", Validators.required],
        location: this.formBuilder.group({
          id: locationId
        })
      },
      { validators: this.dateRange }
    );

    // Retrieve user id from cache
    const editSaleId = window.localStorage.getItem("editSaleId");
    // ---------------------------
    if (!editSaleId) {
      alert("Invalid action.");
      this.router.navigate(["home"]);
      return;
    }

    this.saleService.getSaleById(editSaleId).subscribe(
      data => {
        let sale = {
          id: data.id,
          title: data.title,
          description: data.description,
          number_points: data.number_points,
          photo: data.photo,
          start_date: data.start_date,
          expiration_date: data.expiration_date,
          is_active: data.is_active,
          location: {
            id: data.id
          }
        };
        this.editForm.setValue(sale);
      },
      err => {this.router.navigate(["home"]);}
    );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) {
      return;
    } else {
      this.saleService.updateSale(this.editForm.value).subscribe(
        data => {
          this.router.navigate(["list-sale"]);
        },
        error => {
          // Back-end errors
          const errors = error.error;
          let msg = "Back-end errors";
          for (let key in errors) {
            let value = errors[key];
            msg += "\n" + key + " -> " + value;
          }
          alert(msg);
        }
      );
    }
  }

  get formControls() {
    return this.editForm.controls;
  }

  get photo() {
    return this.editForm.value.photo;
  }

  get formControls_location() {
    var location_control = <FormGroup>this.editForm.get("location");
    return location_control.controls;
  }

  hasErrors() {
    let hasErrors: boolean = false;
    if (this.editForm.errors != null) {
      hasErrors = true;
    }
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.editForm.get(key).errors;
      if (controlErrors != null) {
        console.log(key);
        console.log(controlErrors);
        hasErrors = true;
      }
    });
    Object.keys(this.formControls_location).forEach(key => {
      const controlErrors: ValidationErrors = this.editForm
        .get("location")
        .get(key).errors;
      if (controlErrors != null) {
        console.log(key);
        console.log(controlErrors);
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let valid = true;
      if (file.size > 2000000){ //2MB
        valid = false;
        let control = this.editForm.get("photo");
        control.setErrors({ size: true });
      }else{
        this.formControls["photo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.myFileVariable.nativeElement.value = "";
        this.editForm.value.photo = null;
        return;
      }
      this.editForm.patchValue({
        photo: "../../assets/img/loading.gif",
      });
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        this.editForm.patchValue({
          photo: data.Location,
        });
      });
    }
  }

  dateAfter(formControl: FormControl) {
    const value = formControl.value;
    const date = new Date(value);
    const curr_date = new Date();
    curr_date.setHours(0, 0, 0, 0);
    if (curr_date > date) {
      return {
        after: true
      };
    } else {
      return null;
    }
  }

  dateRange(formGroup: FormGroup) {
    const start_date_input = formGroup.get("start_date").value;
    const expiration_date_input = formGroup.get("expiration_date").value;
    const start_date = new Date(start_date_input);
    const expiration_date = new Date(expiration_date_input);
    if (start_date >= expiration_date) {
      return {
        date_range: true
      };
    } else {
      return null;
    }
  }
}
