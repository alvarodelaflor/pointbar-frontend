import { Component, OnInit } from "@angular/core";
import { Challenge, ChallengeService } from "../services/challenge.service";
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";
import { ClaimChallengeService } from "../services/claim-challenge.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalChallengeClaimComponent } from "../modals/modal-challenge-claim/modal-challenge-claim.component";
import { environment } from "../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";
import { SubscriptionService } from "../services/subscription.service";
import { BarService } from "../services/bar.service";
import { LocationService } from "../services/location.service";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: "app-challenge-list",
  templateUrl: "./challenge-list.component.html",
  styleUrls: ["./challenge-list.component.css"]
})
export class ChallengeListComponent implements OnInit {
  challenges: Challenge[] = [];
  empty = false;

  location_id: any;

  next: string;
  typec = localStorage.getItem("typec");

  previous: string;
  isLoading: boolean = false;
  challenge_button_disabled: boolean = true;
  suscription_msg: string = "checking_for_subscription";

  location_name: any;
  is_owner: boolean = false;
  constructor(
    private challengeService: ChallengeService,
    private router: Router,
    public userService: UserService,
    public translateService: TranslateService,
    private claimChallengeService: ClaimChallengeService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private subscriptionService: SubscriptionService,
    private locationService: LocationService
  ) {
  }

  ngOnInit(): void {
    this.location_name = window.localStorage.getItem("location_name");
    if (!this.location_name) this.router.navigate(["home"]);
    this.location_id = window.localStorage.getItem("locationId");
    if (!this.location_id) this.router.navigate(["home"]);
    var typec = localStorage.getItem("typec");
    var itemsURL =
      environment.apiUrl + "/api/" + typec + "/" + this.location_id;
    const ua = UserService.getuser_account();
    if (ua && ua.authority == "BAR") {
      this.locationService.getLocations().subscribe(
        locations => {
          let loc = locations.filter(l => l.id == this.location_id);
          this.is_owner = loc.length > 0;
          if (this.is_owner){
            this.subscriptionService.getSubscriptions().subscribe(
              data => {
                this.challenge_button_disabled =
                  data.filter(e => e.location.id == this.location_id).length == 0;
                if (this.challenge_button_disabled == false) {
                  this.suscription_msg = "active_subscription_found";
                  this.setItems(itemsURL);
                } else {
                  this.suscription_msg = "active_subscription_not_found";
                }
              },
              err => this.router.navigate(["home"])
            );
          } else{
            this.setItems(itemsURL);
          }
        },
        err => this.router.navigate(["home"])
      );

    } else {
      this.setItems(itemsURL);
    }
  }

  claimChallenge(challenge: Challenge): void {
    this.claimChallengeService.claimChallenge(challenge).subscribe(
      data => {
        this.openModal(true, "");
      },
      err => {
        // ---- Possible errors ----
        // error_challenge_already_completed
        // error_challenge_no_requirements
        // error_challenge_invalid_challenge
        // error_challenge_expire
        // error_challenge_not_started
        // -------------------------
        this.openModal(false, err.error);
      }
    );
  }

  isActive(challenge: Challenge) {
    let start_date = new Date(challenge.start_date);
    start_date = new Date(
      start_date.getFullYear(),
      start_date.getMonth(),
      start_date.getDate()
    );
    let expiration_date = new Date(challenge.expiration_date);
    expiration_date = new Date(
      expiration_date.getFullYear(),
      expiration_date.getMonth(),
      expiration_date.getDate()
    );
    let date = new Date();
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    let res = true;
    if (start_date > date || expiration_date < date) res = false;
    return res;
  }

  createChallenge() {
    localStorage["location_id"] = this.location_id;
    this.router.navigate(["create-challenge"]);
  }

  openModal(success, error_message) {
    const modalRef = this.modalService.open(ModalChallengeClaimComponent);
    modalRef.componentInstance.success = success;
    modalRef.componentInstance.error_message = error_message;
  }

  setItems(url: string) {
    this.empty = false;
    this.isLoading = true;
    this.challengeService.getPaginatedChallenges(url).subscribe(
      response => {
        if (response.count != 0) {
          if (response.next != null) {
            this.next = response.next;
            this.spinner.hide();
          } else {
            this.next = "null";
            this.spinner.hide();
          }
          this.challenges = this.challenges.concat(response.results);
          if (response.previous) {
            // set the components previous property here from the response
            this.previous = response.previous;
          }
        } else {
          this.spinner.hide();
        }

        this.isLoading = false;
      },
      err => {}
    ).add(() => {
      if (this.challenges.length < 1) {
        this.empty = true;
      } else {
        this.empty = false;
      }
    });
  }

  fetchNext() {
    if (this.next != "null" && !this.isLoading) {
      this.spinner.show();
      this.setItems(this.next);
    }
  }

  refresh(): void {
    window.location.reload();
  }

  listChallenges(Typec): void {
    this.empty = false;
    localStorage.setItem("typec", Typec);
    this.challenges = [];
    this.typec = Typec;
    var itemsURL =
      environment.apiUrl + "/api/" + Typec + "/" + this.location_id;
    const ua = UserService.getuser_account();
    if (ua && ua.authority == "BAR") {
      this.locationService.getLocations().subscribe(
        locations => {
          let loc = locations.filter(l => l.id == this.location_id);
          this.is_owner = loc.length > 0;
          if (this.is_owner){
            this.subscriptionService.getSubscriptions().subscribe(
              data => {
                this.challenge_button_disabled =
                  data.filter(e => e.location.id == this.location_id).length == 0;
                if (this.challenge_button_disabled == false) {
                  this.suscription_msg = "active_subscription_found";
                  this.setItems(itemsURL);
                } else {
                  this.suscription_msg = "active_subscription_not_found";
                }
              },
              err => this.router.navigate(["home"])
            );
          }else {
            this.setItems(itemsURL);
          }
        },
        err => this.router.navigate(["home"])
      );

    } else {
      this.setItems(itemsURL);
    }
  }
}
