import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PayloadService, Payload } from "../services/payload.service";
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalPayloadRefresh} from "../modals/modal-payload-refresh/modal-payload-refresh.component";

@Component({
  selector: 'app-payload-show',
  templateUrl: './payload-show.component.html',
  styleUrls: ['./payload-show.component.css']
})
export class PayloadShowComponent implements OnInit {
  qrCode: string;
  points: string;
  scanned: boolean = false;
  maxPointsReached: boolean = false;

  constructor(private router: Router, private payloadService: PayloadService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.qrCode = window.localStorage.getItem('createdPayloadHash');
    this.points = window.localStorage.getItem('createdPayloadPoints');
    // this.maxPointsReached = JSON.parse(window.localStorage.getItem('maxPointsReached'));
    window.localStorage.removeItem('createdPayloadHash');
    window.localStorage.removeItem('createdPayloadPoints');
    this.checkIfScanned();
  }

  private async checkIfScanned(){
    var i = 0;
    while(i < 500){
      await this.delay(3000);
      this.payloadService.getPayloadByCode(parseInt(this.qrCode)).subscribe(data => {
        if(data[0].is_scanned){
          this.scanned = true;
        }
      })
      if(this.scanned){
        break;
      }
      i++;
    }
    this.router.navigate(['create-payload']);
    this.openModal();
  }

  openModal() {
    var modalRef = this.modalService.open(ModalPayloadRefresh);
    modalRef.componentInstance.success = true;
  }

  private delay(ms: number){
  return new Promise(resolve => setTimeout(resolve, ms));
  }

  return(){
    this.router.navigate(['create-payload']);
  }


}
