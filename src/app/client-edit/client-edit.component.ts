import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidationErrors
} from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import { Router } from "@angular/router";
import { Client, ClientService } from "../services/client.service";
import { UserService } from "../services/user.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalExportedUser } from "../modals/modal-exported/modal-exported.component";
import { ModalDelete } from "../modals/modal-delete/modal-delete.component";
import { ModalActorUpdate } from "../modals/modal-actor-update/modal-actor-update.component";
import { ModalDeleteUserComponent } from '../modals/modal-delete-user/modal-delete-user.component';

@Component({
  selector: "app-client-edit",
  templateUrl: "./client-edit.component.html",
  styleUrls: ["./client-edit.component.css"]
})
export class ClientEditComponent implements OnInit {
  @ViewChild("photoFile")
  photoVariable: ElementRef;

  client: Client = {
    name: "",
    phone: "",
    terms_and_conditions: ""
  };

  editForm: FormGroup;
  isSubmitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private clientService: ClientService,
    private cd: ChangeDetectorRef,
    private modalService: NgbModal,
    private userService: UserService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    const phoneRegex =
      "^[0-9]{9,9}$|^(\\+)[0-9]{11,12}$|^((\\()[0-9]{3,3}(\\)) [0-9]{3}-[0-9]{4,4})$";
    const birth_dateRegex = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
    const emailRegex = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)";

    this.editForm = this.formBuilder.group(
      {
        user_account: this.formBuilder.group({
          username: ["", [Validators.required, Validators.maxLength(30)]],
          email: ["", [Validators.required, Validators.pattern(emailRegex)]]
        }),
        id: [""],
        terms_and_conditions: true,
        name: [
          "",
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100)
          ]
        ],
        surname: [
          "",
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100)
          ]
        ],
        phone: ["", [Validators.required, Validators.pattern(phoneRegex)]],
        photo: [""],
        birth_date: [
          "",
          [Validators.required, Validators.pattern(birth_dateRegex)]
        ]
      },
      { validators: this.check_birth_date }
    );

    var user_account = UserService.getuser_account();
    if (user_account) {
      if (
        user_account.authority == "EMPLOYEE" ||
        user_account.authority == "BAR"
      ) {
        this.router.navigate(["home"]);
      }
    } else {
      this.router.navigate(["home"]);
    }

    this.clientService.getClient().subscribe(
      data => this.editForm.setValue(data[0]),
      err => {
        this.router.navigate(["home"]);
      }
    );

    this.clientService.getClient().subscribe(
      data => (this.client = data[0]),
      err => {
        this.router.navigate(["home"]);
      }
    );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    this.clientService.updateClient(this.editForm.value).subscribe(
      data => {
        this.router.navigate(['home']);
        this.openModal(true,"", 3);
      },
      error => {
        this.openModal(false, error.error, 3);
      }
    );
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach(key => {
      const controlErrors: ValidationErrors = this.editForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  get formControls() {
    return this.editForm.controls;
  }

  get user_accountControls() {
    var user_account = <FormGroup>this.editForm.get("user_account");
    return user_account.controls;
  }

  get photo() {
    return this.editForm.value["photo"];
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      if (!file.type.includes("image")) {
        this.photoVariable.nativeElement.value = "";
        this.editForm.value.photo = null;
        return;
      }
      reader.onload = () => {
        this.editForm.patchValue({
          photo: reader.result
        });

        this.cd.markForCheck();
      };
    }
  }

  check_birth_date(formGruop: FormGroup) {
    let birth_date = formGruop.get("birth_date").value;
    let birth_date_aux = birth_date.split("-", 3);
    let day_birth_date = birth_date_aux[2];
    let month_birth_date = birth_date_aux[1];
    let year_birth_date = birth_date_aux[0];

    let date = new Date().toLocaleDateString();
    let date_aux = date.split("/", 3);
    let day_now = date_aux[0];
    let month_now = date_aux[1];
    let year_now = date_aux[2];

    if (month_now.length == 1) {
      month_now = 0 + month_now;
    }

    if (+year_now - year_birth_date < 13) {
      formGruop.get("birth_date").setErrors({
        ErrorDay: true
      });
    } else {
      if (month_now < month_birth_date && +year_now - year_birth_date == 13) {
        formGruop.get("birth_date").setErrors({
          ErrorDay: true
        });
      } else {
        if (
          day_now < day_birth_date &&
          +year_now - year_birth_date == 13 &&
          month_now == month_birth_date
        ) {
          formGruop.get("birth_date").setErrors({
            ErrorDay: true
          });
        } else {
          return null;
        }
      }
    }
  }

  onDelete() {
    if (this.hasErrors()) return;

    this.clientService.deleteBarAnon(this.editForm.value).subscribe(data => {
      this.userService.logout();
      this.router.navigate(["home"]);
    },
      error => {
        this.userService.logout();
        this.router.navigate(["home"]);
        this.openModal(false, error.error, 2);
      });
  }

  onExport(id: number) {
    if (this.hasErrors()) return;
    this.clientService.export(id).subscribe(data => {
      this.openModal(true, "", 1);
    },
      error => {
        this.openModal(false, error.error, 1);
      });
  }

  openModal(success, error_message, id) {
    if(id == 1){
       var modalRef = this.modalService.open(ModalExportedUser);
    } else if (id == 2){
       var modalRef = this.modalService.open(ModalDelete);
    } else {
       var modalRef = this.modalService.open(ModalActorUpdate);
    }
    modalRef.componentInstance.success = success;
    var t = typeof error_message;
    if (['boolean', 'number', 'string', 'symbol', 'function'].indexOf(t) == -1){
      var err_msg = String(error_message.detail);
    } else {
      var err_msg = String(error_message);
    }
    modalRef.componentInstance.error_message = err_msg;
  }

  deleteModal() {
    const modalRef = this.modalService.open(ModalDeleteUserComponent);
    modalRef.result.then((result) => {
      // Close
    }, (reason) => {
      // Delete
      if (reason == "save")
        this.onDelete();
    });
  }
}
