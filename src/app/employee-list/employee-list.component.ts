import { Component, OnInit } from '@angular/core';
import { Location, LocationService } from '../services/location.service';
import {
  Employee,
  EmployeeChange,
  EmployeeService
} from '../services/employee.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: EmployeeChange[] = [];
  locations: Location[] = [];
  employeesURL = environment.apiUrl + '/api/employee/myemployee';
  next: string;
  previous: string;
  isLoading = false;
  empty: boolean = false;
  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private locationService: LocationService,
    private translateService: TranslateService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.getLocations();
    this.getPaginatedEmployees(this.employeesURL);
  }
  getEmployees(): void {
    this.employeeService.getEmployees2().subscribe(
      employees => {
        this.employees = employees;
      },
      err => {
        this.router.navigate(['home']);
      }
    );
  }

  getLocations(): void {
    this.locationService.getLocations().subscribe(
      locations => {
        this.locations = locations;
      },
      err => {
        this.router.navigate(['home']);
      }
    );
  }

  changeStatus(employee: EmployeeChange): void {

    if (this.translateService.currentLang === 'en') {
      if (employee.is_working) {
        if (confirm('Are you sure you want to deactivate ' + employee.name + ' ?')) {
          this.employeeService.changeEmployeeStatus(employee).subscribe(data => {
            this.locations = [];
            this.getLocations();
            this.employees = [];
            this.getPaginatedEmployees(this.employeesURL);
          });
        }
      } else {
        if (confirm('Are you sure you want to activate ' + employee.name + ' ?')) {
          this.employeeService.changeEmployeeStatus(employee).subscribe(data => {
            this.locations = [];
            this.getLocations();
            this.employees = [];
            this.getPaginatedEmployees(this.employeesURL);
          });
        }
      }
    } else {
      if (employee.is_working) {
        if (confirm('¿Está seguro de que quiere desactivar a ' + employee.name + ' ?')) {
          this.employeeService.changeEmployeeStatus(employee).subscribe(data => {
            this.locations = [];
            this.getLocations();
            this.employees = [];
            this.getPaginatedEmployees(this.employeesURL);
          });
        }
      } else {
        if (confirm('¿Está seguro de que quiere activar a ' + employee.name + ' ?')) {
          this.employeeService.changeEmployeeStatus(employee).subscribe(data => {
            this.locations = [];
            this.getLocations();
            this.employees = [];
            this.getPaginatedEmployees(this.employeesURL);
          });
        }
      }
    }
  }

  resetGrantedPoints(employee: EmployeeChange): void {

    if (this.translateService.currentLang === "en") {

      if (confirm("Are you sure you want to deactivate " + employee.name + " ?")) {
        this.employeeService.resetGrantedPoints(employee).subscribe(data => {
          console.log(employee)
          this.getLocations();
          this.getEmployees();
          window.location.reload();
        })
      }
    } else {

      if (confirm("¿Está seguro que quiere resetear a " + employee.name + " ?")) {
        this.employeeService.resetGrantedPoints(employee).subscribe(data => {
          console.log(employee)
          this.getLocations();
          this.getEmployees();
          window.location.reload();
        })
      }
    }

  }

  changeLocation(employee: EmployeeChange, location: string): void {
    if (
      location != null &&
      location !== 'Cambiar restaurante' &&
      location !== 'Change location'
    ) {
      if (this.translateService.currentLang === 'en') {
        if (confirm('Are you sure you want to change the location?')) {
          this.employeeService
            .changeEmployeeLocation(employee, location)
            .subscribe(data => {
              this.getLocations();
              this.employees = [];
              this.getPaginatedEmployees(this.employeesURL);
            });
        }
      } else {
        if (confirm('¿Está seguro de que quiere cambiar el establecimiento?')) {
          this.employeeService
            .changeEmployeeLocation(employee, location)
            .subscribe(data => {
              this.getLocations();
              this.employees = [];
              this.getPaginatedEmployees(this.employeesURL);
            });
        }
      }
    }
  }

  showLocation(locationId: string): void {
    localStorage.removeItem('locationId');
    localStorage.setItem('locationId', locationId);
    this.router.navigate(['location-show']);
  }

  editEmployee(employee: Employee): void {
    localStorage.removeItem('employeeId');
    localStorage.setItem('employeeId', employee.id.toString());
    this.router.navigate(['employee-edit']);
  }

  getPaginatedEmployees(url: string) {
    this.isLoading = true;
    this.empty = false;
    this.employeeService.getPaginatedEmployees(url).subscribe(response => {
      if (response.next != null) {
        this.next = response.next;
        this.spinner.hide();
      } else {
        this.next = 'null';
        this.spinner.hide();
      }
      this.employees = this.employees.concat(response.results);

      if (this.employees.length <= 0) {
        this.empty = true;
      }

      if (response.previous) {
        this.previous = response.previous;
      }
      this.isLoading = false;
    }, err => {this.router.navigate(["home"]);});
  }

  fetchNext() {
    if (this.next !== 'null' && !this.isLoading) {
      this.spinner.show();
      this.getPaginatedEmployees(this.next);
    }
  }
}
