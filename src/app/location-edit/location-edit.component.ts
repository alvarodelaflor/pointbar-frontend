import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { Location, LocationService } from "../services/location.service";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidationErrors,
} from "@angular/forms";
import { Router } from "@angular/router";
import { UploadService } from "../services/upload.service";

@Component({
  selector: "app-location-edit",
  templateUrl: "./location-edit.component.html",
  styleUrls: ["./location-edit.component.css"],
})
export class LocationEditComponent implements OnInit {
  location: Location;
  editForm: FormGroup;
  isSubmitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private locationService: LocationService,
    private cd: ChangeDetectorRef,
    private uploadService: UploadService
  ) {}
  @ViewChild("logoFile")
  logoVariable: ElementRef;

  @ViewChild("photoFile")
  photoVariable: ElementRef;

  ngOnInit(): void {
    const urlRegex = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
    const emailRegex = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)';
    this.editForm = this.formBuilder.group({
      id: [],
      name: ["", [Validators.required ,Validators.maxLength(60)]],
      description: ["", [Validators.required ,Validators.maxLength(250)]],
      email: ["", [Validators.required, Validators.pattern(emailRegex)]],
      website: ["", [Validators.required, Validators.pattern(urlRegex)]],
      logo: [null],
      photo: [null],
      latitude: ["", Validators.required],
      longitude: ["", Validators.required],
      street: ["", Validators.required],
      city: ["", Validators.required],
      state: ["", Validators.required],
      postal_code: ["", Validators.required],
      country: ["", Validators.required],
      is_active: ["", Validators.required],
      bar: this.formBuilder.group({ id: [], name: [""] }),
    });

    // Retrieve user id from cache
    const locationId = window.localStorage.getItem("locationId");
    // ---------------------------
    if (!locationId) {
      alert("Invalid action.");
      this.router.navigate(["home"]);
      return;
    }

    this.locationService.getLocationById(+locationId).subscribe(
      (data) => this.editForm.setValue(data),
      (err) => {
        this.router.navigate(["home"]);
      }
    );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    this.locationService.updateLocation(this.editForm.value).subscribe(
      (data) => {
        this.router.navigate(["location-list"]); // We go to our location list
      },
      (error) => {
        this.router.navigate(["home"]); // Backend sent an error
      }
    );
  }

  get formControls() {
    return this.editForm.controls;
  }

  get logo() {
    return this.editForm.value["logo"];
  }

  get photo() {
    return this.editForm.value["photo"];
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let valid = true;
      if (file.size > 2000000){ //2MB
        valid = false;
        let control = this.editForm.get("logo");
        control.setErrors({ size: true });
      }else{
        this.formControls["logo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.logoVariable.nativeElement.value = "";
        this.editForm.value.logo = null;
        return;
      }
      this.editForm.patchValue({
        logo: "../../assets/img/loading.gif",
      });
      this.cd.markForCheck();
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        this.editForm.patchValue({
          logo: data.Location,
        });
        this.cd.markForCheck();
      });
    }
  }

  addFiles(event) {
    // files is not a regular Array
    var fileList: File[] = Array.from(event.target.files);
    let res = [];
    let fail = false;
    res = Array(fileList.length).fill("../../assets/img/loading.gif");
    this.editForm.patchValue({
      photo: res,
    });
    this.cd.markForCheck();
    fileList.forEach((file) => {
      let valid = true;
      if (file.size > 2000000) {
        //2MB
        valid = false;
        let control = this.editForm.get("photo");
        control.setErrors({ size: true });
      } else {
        this.formControls["photo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.photoVariable.nativeElement.value = "";
        this.editForm.value.photo = null;
        fail = true;
        return;
      }
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        res.shift();
        res.push(data.Location);
        if (fail) res = [];
        this.editForm.patchValue({
          photo: res,
        });
        this.cd.markForCheck();
      });
    });
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach((key) => {
      const controlErrors: ValidationErrors = this.editForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

}
