import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { LocationCreateComponent } from "./location-create.component";
import { ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "../app-routing.module";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { createTranslateLoader } from "../app.module";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe("LocationCreateComponent", () => {
  let component: LocationCreateComponent;
  let fixture: ComponentFixture<LocationCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        AppRoutingModule,
        HttpClientModule,
        TranslateModule.forRoot({
          defaultLanguage: "en",
          loader: {
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [LocationCreateComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
