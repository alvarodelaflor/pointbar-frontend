import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidationErrors,
} from "@angular/forms";
import { Router } from "@angular/router";
import { LocationService } from "../services/location.service";
import { AgmCoreModule } from "@agm/core";
import { UploadService } from "../services/upload.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalLocationCreateComponent } from '../modals/modal-location-create/modal-location-create.component';

declare var google: any;

@Component({
  selector: "app-location-create",
  templateUrl: "./location-create.component.html",
  styleUrls: ["./location-create.component.css"],
})
export class LocationCreateComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private locationService: LocationService,
    private cd: ChangeDetectorRef,
    private uploadService: UploadService,
    private modalService: NgbModal
  ) {}
  @ViewChild("logoFile")
  logoVariable: ElementRef;

  @ViewChild("photoFile")
  photoVariable: ElementRef;

  markers = [];

  lat: number = 37.389437;
  lng: number = -5.984443;
  map: any;

  createForm: FormGroup;
  isSubmitted: boolean = false;

  ngOnInit(): void {
    const urlRegex = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
    const emailRegex = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)';
    this.createForm = this.formBuilder.group({
      id: [],
      name: ["", [Validators.required ,Validators.maxLength(60)]],
      description: ["", [Validators.required ,Validators.maxLength(250)]],
      email: ["", [Validators.required, Validators.pattern(emailRegex)]],
      website: ["", [Validators.required, Validators.pattern(urlRegex)]],
      logo: [null],
      photo: [null],
      latitude: ["", Validators.required],
      longitude: ["", Validators.required],
      street: ["", Validators.required],
      city: ["", Validators.required],
      state: ["", Validators.required],
      postal_code: ["", Validators.required],
      country: ["", Validators.required],
    });
  }

  findMe(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        this.map.setCenter(pos);
        var marker = new google.maps.Marker({
          position: pos,
          map: this.map,
          title: "Tu ubicación",
          icon: "https://i.ibb.co/r7tf4rn/placeholder.png",
        });
        this.markers.push(marker);
      });
    }
  }

  mapReady(event: any) {
    this.map = event;
    this.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(
      document.getElementById("Location")
    );

    let geocoder = new google.maps.Geocoder();

    this.map.addListener("click", (event) => {
      var myLatLng = event.latLng;

      let request = {
        latLng: myLatLng,
      };

      geocoder.geocode(
        request,
        (results, status) => {
          this.createForm
            .get("street")
            .setValue(
              results[0].address_components
                .filter((x) => x.types.includes("route"))
                [0].long_name +
                ", " +
                results[0].address_components
                  .filter((x) => x.types.includes("street_number"))
                  [0].long_name
            );
          this.createForm
            .get("city")
            .setValue(
              results[0].address_components
                .filter((x) => x.types.includes("locality"))
                [0].long_name
            );
          this.createForm
            .get("state")
            .setValue(
              results[0].address_components
                .filter((x) => x.types.includes("administrative_area_level_1"))
                [0].long_name
            );
          this.createForm
            .get("postal_code")
            .setValue(
              results[0].address_components
                .filter((x) => x.types.includes("postal_code"))
                [0].long_name
            );
          this.createForm
            .get("country")
            .setValue(
              results[0].address_components
                .filter((x) => x.types.includes("country"))
                [0].long_name
            );
        },
        (error) => {
          console.log(
            "Error code: " +
              error.code +
              "<br /> Error message: " +
              error.message
          );
        }
      );

      this.addMarker(event.latLng);
      this.createForm.get("latitude").setValue(event.latLng.lat().toFixed(6));
      this.createForm.get("longitude").setValue(event.latLng.lng().toFixed(6));
    });

    this.findMe();
  }

  addMarker(location) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    var marker = new google.maps.Marker({
      position: location,
      map: this.map,
    });
    this.markers.push(marker);
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.hasErrors()) return;
    this.locationService.createLocation(this.createForm.value).subscribe(
      (data) => {
        this.router.navigate(["location-list"]); // We go to our location list
      },
      (error) => {
        this.router.navigate(["home"]); // Backend sent an error
      }
    );
  }

  get formControls() {
    return this.createForm.controls;
  }

  get logo() {
    return this.createForm.value["logo"];
  }

  get photo() {
    return this.createForm.value["photo"];
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let valid = true;
      if (file.size > 2000000) {
        //2MB
        valid = false;
        let control = this.createForm.get("logo");
        control.setErrors({ size: true });
      } else {
        this.formControls["logo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.logoVariable.nativeElement.value = "";
        this.createForm.value.logo = null;
        return;
      }
      this.createForm.patchValue({
        logo: "../../assets/img/loading.gif",
      });
      this.cd.markForCheck();
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        this.createForm.patchValue({
          logo: data.Location,
        });
        this.cd.markForCheck();
      });
    }
  }

  addFiles(event) {
    // files is not a regular Array
    var fileList: File[] = Array.from(event.target.files);
    let res = [];
    let fail = false;
    res = Array(fileList.length).fill("../../assets/img/loading.gif");
    this.createForm.patchValue({
      photo: res,
    });
    this.cd.markForCheck();
    fileList.forEach((file) => {
      let valid = true;
      if (file.size > 2000000) {
        //2MB
        valid = false;
        let control = this.createForm.get("photo");
        control.setErrors({ size: true });
      } else {
        this.formControls["photo"].setErrors(null);
      }
      if (!file.type.includes("image") || !valid) {
        this.photoVariable.nativeElement.value = "";
        this.createForm.value.photo = null;
        fail = true;
        return;
      }
      let bucket = this.uploadService.uploadFile(file, (err, data) => {
        if (err) {
          console.log("There was an error uploading your file: ", err);
        }
        res.shift();
        res.push(data.Location);
        if (fail) res = [];
        this.createForm.patchValue({
          photo: res,
        });
        this.cd.markForCheck();
      });
    });
  }

  hasErrors() {
    let hasErrors: boolean = false;
    Object.keys(this.formControls).forEach((key) => {
      const controlErrors: ValidationErrors = this.createForm.get(key).errors;
      if (controlErrors != null) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  openModal() {
    const modalRef = this.modalService.open(ModalLocationCreateComponent);
    modalRef.componentInstance.location = this.createForm.value;
    modalRef.result.then((result) => {
      // Close
    }, (reason) => {
      // Save
      if (reason == "save")
        this.onSubmit();
    });
  }
}
