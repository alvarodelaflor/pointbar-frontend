import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Pointbar';

  constructor(public translate: TranslateService, private router: Router, public userService: UserService) {
    let lang = localStorage.getItem("currentLanguage");
    translate.setDefaultLang('es');
    if(lang) {
      this.translate.use(lang);
      localStorage.removeItem("currentLanguage");
    }
    else 
    translate.use('es');
  }
  ngOnInit(): void {
  }

  get user_account(){
    return UserService.getuser_account();
  }

  switchLanguage(lang) {
    this.translate.use(lang);
  }

  refreshToken() {
    this.userService.refreshToken();
  }

  logout() {
    this.userService.logout();
  }
}
